<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>File Manager</title>
	<link rel="shortcut icon" href="../../assets/demo/fileManager/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="../../assets/demo/fileManager/default.css">
</head>
<body>

<div id="file-manager"></div>

<script src="../../assets/demo/fileManager/app-es5-min.js"></script>
<script type="text/javascript">
  new engine.fileManager.Application({
    wrapper: document.getElementById('file-manager')
  })
</script>

</body>
</html>
