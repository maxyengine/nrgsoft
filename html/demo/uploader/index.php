<?php
if (!empty($_GET['r'])) {
    require __DIR__.'/server/run.php';
    exit(0);
}
?><!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<link rel="shortcut icon" href="./favicon.ico"/>
	<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no"/>
	<title>Uploader Demo | NrgSoft</title>
	<link href="./static/css/default.css" rel="stylesheet" id="theme">
	<style type="text/css">
		* {
			padding: 0;
			margin: 0;
		}
		
		body {
			font-family: "Open Sans", arial, sans-serif;
			background: #2c3438;
			padding-top: 50px;
		}
		
		.breadcrumbs {
			font-size: 1.2em;
			margin-bottom: 40px;
			text-align: center;
			color: #777777;
		}
		
		.breadcrumbs > a {
			padding: 7px;
			text-decoration: none;
			color: #ffffff;
		}
		
		.breadcrumbs > a:hover {
			color: #4d88ff;
		}
		
		.breadcrumbs > span {
			padding: 7px;
			color: #4d88ff;
		}

		.theming {
			margin: 10px auto;
			width: 279px;
			color: #fff
		}

		.theming input {
			margin: 0 10px;
			cursor: pointer;
		}

		.theming label {
			margin: 10px;
			cursor: pointer;
		}

		.credentials {
			margin: 10px auto;
			width: 400px;
			color: #fff;
			padding: 10px 0;
		}
		
		#app {
			margin: 0 auto;
			min-width: 279px;
			max-width: 728px;
		}
	</style>
</head>
<body>

<div class="breadcrumbs">
	<a href="../../">Home</a> /
	<a href="../../products/uploader">Landing</a> /
	<a href="../../docs/uploader">Docs</a> /
	<span>Demo</span>
</div>

<div class="theming">
	<form id="themeToggle">
		<label>
			<input name="theme" type="radio" value="default" checked>Default
		</label>
		<label>
			<input name="theme" type="radio" value="dark">Dark
		</label>
	</form>
</div>

<div class="credentials">Email: admin@admin.com &emsp;/&emsp; Password: admin</div>

<div id="app"></div>

<script src="./static/js/main.js"></script>

<script>
  // Theming
  var themeToggle = document.getElementById('themeToggle').theme
  var link = document.getElementById('theme')

  for (var i = 0; i < themeToggle.length; i++) {
    themeToggle[i].addEventListener('change', function (event) {
      link.href = './static/css/' + event.target.value + '.css'
    })
  }

</script>

</body>
</html>