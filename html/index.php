<?php

function getRoot()
{
    $root = str_replace('\\', '/', dirname($_SERVER['PHP_SELF']));

    return '/' === $root ? '' : $root;
}

function getPath($root)
{
    $defaultPath = 'home';
    $pageNotFound = '../pages/404.php';

    $uri = $_SERVER['REQUEST_URI'];
    $path = trim(substr($uri, strlen($root)), '/');
    $fileName = basename(__FILE__);

    if (empty($path) || $path === $fileName) {
        $path = $defaultPath;
    }

    $path = "../pages/{$path}.php";

    return is_file($path) ? $path : $pageNotFound;
}

function tpl($name, $vars = [])
{
    extract($vars);
    $root = getRoot();
    require "../tpls/{$name}.php";
}

$root = getRoot();
require getPath($root);



