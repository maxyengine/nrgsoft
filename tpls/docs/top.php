<?php tpl('top', compact('title', 'description', 'keywords', 'favicon')) ?>

<link rel="stylesheet" href="<?= $root ?>/assets/docs/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= $root ?>/assets/docs/plugins/prism/prism.css">
<link rel="stylesheet" href="<?= $root ?>/assets/docs/plugins/elegant_font/css/style.css">
<link id="theme-style" rel="stylesheet" href="<?= $root ?>/assets/docs/css/styles.css">
</head>

<body class="body-blue">
<div class="page-wrapper">