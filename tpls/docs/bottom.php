</div><!--//page-wrapper-->

<footer id="footer" class="footer text-center">
	<div class="container">
		<small class="copyright">&copy; <?= date('Y') ?> NrgSoft</small>
	</div>
</footer>

<script type="text/javascript" src="<?= $root ?>/assets/docs/plugins/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?= $root ?>/assets/docs/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $root ?>/assets/docs/plugins/prism/prism.js"></script>
<script type="text/javascript" src="<?= $root ?>/assets/docs/plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?= $root ?>/assets/docs/plugins/stickyfill/dist/stickyfill.min.js"></script>
<script type="text/javascript" src="<?= $root ?>/assets/docs/js/main.js"></script>

</body>
</html>