<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136331733-1"></script>
	<script>
    window.dataLayer = window.dataLayer || []

    function gtag () {dataLayer.push(arguments)}

    gtag('js', new Date())
    gtag('config', 'UA-136331733-1')
	</script>

	<title><?= $title ?? 'NrgSoft' ?></title>

	<meta charset="utf-8">
	<!--[if IE]>
	<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="<?= $description ?? '' ?>">
	<meta name="keywords" content="<?= $keywords ?? '' ?>"/>

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700%7CLato:300,400,400i,700' rel='stylesheet'>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	      integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	<link rel="shortcut icon" href="<?= $favicon ?? "{$root}/assets/main/img/favicon.ico" ?>">