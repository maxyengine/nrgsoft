<!-- Footer -->
<footer class="footer bg-dark">
	<div class="footer__bottom top-divider">
		<div class="container text-center">
            <span class="copyright">
              &copy; <?= date('Y') ?> NrgSoft
            </span>
		</div>
	</div> <!-- end footer bottom -->
</footer> <!-- end footer -->


<div id="back-to-top">
	<a href="#top"><i class="ui-arrow-up"></i></a>
</div>