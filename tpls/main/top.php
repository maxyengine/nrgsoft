<?php tpl('top', compact('title', 'description', 'keywords', 'favicon')) ?>

<link rel="stylesheet" href="<?= $root ?>/assets/main/css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?= $root ?>/assets/main/css/font-icons.css"/>
<link rel="stylesheet" href="<?= $root ?>/assets/main/css/style.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
</head>

<body>

<!-- Preloader -->
<div class="loader-mask">
	<div class="loader">
		"Loading..."
	</div>
</div>