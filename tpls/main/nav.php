<ul class="nav__menu nav__menu--inline">
    <li>
        <a href="<?=$root?>/">Home</a>
    </li>
    <li class="nav__dropdown active">
        <a href="#">Products</a>
        <i class="ui-arrow-down nav__dropdown-trigger"></i>
        <ul class="nav__dropdown-menu">
            <li><a href="<?=$root?>/products/fileManager">File Manager</a></li>
            <li><a href="<?=$root?>/products/uploader">Uploader</a></li>
        </ul>
    </li>
		<li class="nav__dropdown active">
			<a href="#">Docs</a>
			<i class="ui-arrow-down nav__dropdown-trigger"></i>
			<ul class="nav__dropdown-menu">
				<li><a href="<?=$root?>/docs/fileManager">File Manager</a></li>
				<li><a href="<?=$root?>/docs/uploader">Uploader</a></li>
			</ul>
		</li>
</ul> <!-- end menu -->