<?php
tpl(
    'docs/top',
    [
        'title' => 'File Manager | NrgSoft',
        'description' => 'File manager documentation',
        'keywords' => 'file manager, file storage',
        'favicon' => "{$root}/assets/main/img/file-manager/favicon.ico",
    ]
);
?>
	<!-- ******Header****** -->
	<header id="header" class="header">
		<div class="container">
			<div class="branding">
				<h1 class="logo">
					<span aria-hidden="true" class="icon_documents_alt icon"></span>
					<span class="text-highlight">$nrg.</span><span class="text-bold">fileManager</span>
				</h1>

			</div><!--//branding-->

			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?= $root ?>">Home</a></li>
				<li class="breadcrumb-item"><a href="<?= $root ?>/products/fileManager">File Manager</a></li>
				<li class="breadcrumb-item active">Documentation</li>
			</ol>

			<div class="top-search-box">
				<a href="https://codecanyon.net/item/engine-file-manager/20829537" class="btn btn-blue" target="_blank">
					<i class="fas fa-download"></i> Download</a>
			</div>

		</div><!--//container-->
	</header><!--//header-->
	<div class="doc-wrapper">
		<div class="container">
			<div id="doc-header" class="doc-header text-center">
				<h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Quick Start</h1>
				<div class="meta"><i class="far fa-clock"></i> Last updated: July 18th, 2018</div>
			</div><!--//doc-header-->
			<div class="doc-body row">
				<div class="doc-content col-md-9 col-12 order-1">
					<div class="content-inner">

						<section id="requirements-section" class="doc-section">
							<h2 class="section-title">Requirements</h2>
							<div class="section-block">
								<ul class="list">
									<li>A web server with PHP support, for example:
										<ul>
											<li><a href="https://httpd.apache.org/" target="_blank">Apache</a> 2.0 or greater</li>
											<li><a href="https://nginx.com/" target="_blank">Nginx</a> 1.1 or greater</li>
										</ul>
									</li>
									<li><a href="https://php.net/" target="_blank">PHP</a> 7.x.x</li>
									<li>PHP extensions are required:
										<ul>
											<li>php-mbstring</li>
											<li>php-mcrypt</li>
											<li>php-zip</li>
										</ul>
									</li>
								</ul>
							</div>
						</section>

						<section id="installation-section" class="doc-section">
							<h2 class="section-title">Installation</h2>
							<div id="step1" class="section-block">
								<p>
									We will give an example of installation on
									<a href="https://httpd.apache.org/" target="_blank">Apache</a> server on
									<a href="https://www.ubuntu.com/" target="_blank">Ubuntu</a> operating system.
									The default document root for Apache is `/var/www/html` (Ubuntu 14.04 or later).
								</p>

								<div class="code-block">
									<h6>Unpack archive to any directory on your server:</h6>
									<p><code>unzip engine-fm.zip</code></p>
								</div>

								<div class="code-block">
									<h6>Copy the contents to /var/www:</h6>
									<p><code>cp -a ./engine-fm/ ./html/ /var/www/</code></p>

									<div class="callout-block callout-success">
										<div class="icon-holder">
											<svg class="svg-inline--fa fa-thumbs-up fa-w-16" aria-hidden="true" focusable="false"
											     data-prefix="fas" data-icon="thumbs-up" role="img" xmlns="http://www.w3.org/2000/svg"
											     viewBox="0 0 512 512" data-fa-i2svg="">
												<path fill="currentColor"
												      d="M104 224H24c-13.255 0-24 10.745-24 24v240c0 13.255 10.745 24 24 24h80c13.255 0 24-10.745 24-24V248c0-13.255-10.745-24-24-24zM64 472c-13.255 0-24-10.745-24-24s10.745-24 24-24 24 10.745 24 24-10.745 24-24 24zM384 81.452c0 42.416-25.97 66.208-33.277 94.548h101.723c33.397 0 59.397 27.746 59.553 58.098.084 17.938-7.546 37.249-19.439 49.197l-.11.11c9.836 23.337 8.237 56.037-9.308 79.469 8.681 25.895-.069 57.704-16.382 74.757 4.298 17.598 2.244 32.575-6.148 44.632C440.202 511.587 389.616 512 346.839 512l-2.845-.001c-48.287-.017-87.806-17.598-119.56-31.725-15.957-7.099-36.821-15.887-52.651-16.178-6.54-.12-11.783-5.457-11.783-11.998v-213.77c0-3.2 1.282-6.271 3.558-8.521 39.614-39.144 56.648-80.587 89.117-113.111 14.804-14.832 20.188-37.236 25.393-58.902C282.515 39.293 291.817 0 312 0c24 0 72 8 72 81.452z"></path>
											</svg>
										</div>
										<div class="content">
											If you are copying to a location not owned by the current user then you should use `sudo`.
										</div>
									</div>
								</div>

								<p><code>sudo cp -a ./engine-fm/ ./html/ /var/www/</code></p>

								<div class="code-block">
									<h6>Ultimately you will get the next structure of folders:</h6>
									<p>
									<pre>var/www
       /engine-fm
                 /runtime
                 /settings
                 /vendors
                 /app.php
       /html
            /engine-fm
                      /doc
                      /js
                      /styles
                      /.htaccess
                      /favicon.ico
                      /index.html
                      /server.php</pre>
									</p>
								</div>

							</div>
						</section>

						<section id="settings-section" class="doc-section">
							<h2 class="section-title">Settings</h2>
							<div class="section-block">
								<p>The main configuration file location is `%application-dir%/engine-fm/settings/public.php`</p>

								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
										<tr>
											<th>Name</th>
											<th>Default</th>
											<th>Description</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td>serverUrl</td>
											<td>.</td>
											<td>URL for server side of the application</td>
										</tr>
										<tr>
											<td>storagePath</td>
											<td>%application-dir%/engine-fm/root</td>
											<td>Root of file storage</td>
										</tr>
										<tr>
											<td>storageMaxSize</td>
											<td>5 GB</td>
											<td>Maximum allowed size for storage</td>
										</tr>
										<tr>
											<td>uploadMaxFileSize</td>
											<td>1 MB</td>
											<td>Maximum allowed uploaded file size. Note that php.ini setting has precedence over this
												setting
											</td>
										</tr>
										<tr>
											<td>maxNumberOfUploadFiles</td>
											<td>5</td>
											<td>Maximum number of files to upload</td>
										</tr>
										<tr>
											<td>allowMimeTypes</td>
											<td>null (all allowed) or array of mime types</td>
											<td>Allowed mime types (for uploading)</td>
										</tr>
										<tr>
											<td>denyMimeTypes</td>
											<td>null (all allowed) or array of mime types</td>
											<td>Denied mime types (for uploading)</td>
										</tr>
										<tr>
											<td>icons</td>
											<td>See list in configuration file</td>
											<td>List of icons used by toolbar buttons and different mime file types</td>
										</tr>
										<tr>
											<td>defaultIcon</td>
											<td>icon-doc</td>
											<td>Icon by default for unknown mime file type</td>
										</tr>
										<tr>
											<td colspan="3">hotKeys</td>
										</tr>
										<tr>
											<td>openKey</td>
											<td>Enter</td>
											<td>Open a selected file</td>
										</tr>
										<tr>
											<td>renameKey</td>
											<td>Alt+r</td>
											<td>Rename a selected file</td>
										</tr>
										<tr>
											<td>permissionsKey</td>
											<td>Alt+p</td>
											<td>Set selected file permissions</td>
										</tr>
										<tr>
											<td>copyKey</td>
											<td>Alt+c</td>
											<td>Copy selected file(s)</td>
										</tr>
										<tr>
											<td>moveKey</td>
											<td>Alt+m</td>
											<td>Move selected file(s)</td>
										</tr>
										<tr>
											<td>trashKey</td>
											<td>Delete</td>
											<td>Trash selected file(s)</td>
										</tr>
										<tr>
											<td>removeKey</td>
											<td>Shift+Delete</td>
											<td>Remove selected file(s)</td>
										</tr>
										<tr>
											<td>createFolderKey</td>
											<td>Alt+f</td>
											<td>Create a new folder</td>
										</tr>
										<tr>
											<td>createHyperlinkKey</td>
											<td>Alt+h</td>
											<td>Create a new hyperlink</td>
										</tr>
										<tr>
											<td>uploadKey</td>
											<td>Alt+u</td>
											<td>Upload file(s)</td>
										</tr>
										<tr>
											<td>downloadKey</td>
											<td>Alt+d</td>
											<td>Download file(s)</td>
										</tr>
										<tr>
											<td>searchKey</td>
											<td>Alt+s</td>
											<td>Search file(s)</td>
										</tr>
										<tr>
											<td>switchLeftKey</td>
											<td>Ctrl+37</td>
											<td>Switch on left panel</td>
										</tr>
										<tr>
											<td>switchRightKey</td>
											<td>Ctrl+37</td>
											<td>Switch on right panel</td>
										</tr>
										<tr>
											<td>nextItemKey
											</td>
											<td>40 (arrow to down)</td>
											<td>Select next file</td>
										</tr>
										<tr>
											<td>prevItemKey
											</td>
											<td>38 (arrow to up)</td>
											<td>Select prev file</td>
										</tr>
										<tr>
											<td>firstItemKey</td>
											<td>37-Ctrl (arrow to left without Ctrl)</td>
											<td>Select first file</td>
										</tr>
										<tr>
											<td>lastItemKey</td>
											<td>39-Ctrl (arrow to right without Ctrl)</td>
											<td>Select last file</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
						</section>

					</div>
				</div>

				<div class="doc-sidebar col-md-3 col-12 order-0 d-none d-md-flex">
					<div id="doc-nav" class="doc-nav">
						<nav id="doc-menu" class="nav doc-menu flex-column sticky">
							<a class="nav-link scrollto" href="#requirements-section">Requirements</a>
							<a class="nav-link scrollto" href="#installation-section">Installation</a>
							<a class="nav-link scrollto" href="#settings-section">Settings</a>
						</nav><!--//doc-menu-->
					</div>
				</div><!--//doc-sidebar-->
			</div><!--//doc-body-->
		</div><!--//container-->
	</div><!--//doc-wrapper-->

<?php
tpl('docs/bottom');
?>