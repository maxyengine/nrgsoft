<?php
tpl(
    'docs/top',
    [
        'title' => 'Uploader | NrgSoft',
        'description' => 'Uploader documentation',
        'keywords' => 'uploader, multiupload',
        'favicon' => "{$root}/assets/main/img/uploader/favicon.ico",
    ]
);
?>
	<!-- ******Header****** -->
	<header id="header" class="header">
		<div class="container">
			<div class="branding">
				<h1 class="logo">
					<span aria-hidden="true" class="icon_documents_alt icon"></span>
					<span class="text-highlight">$nrg.</span><span class="text-bold">uploader</span>
				</h1>

			</div><!--//branding-->

			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?= $root ?>">Home</a></li>
				<li class="breadcrumb-item"><a href="<?= $root ?>/products/uploader">Uploader</a></li>
				<li class="breadcrumb-item active">Documentation</li>
			</ol>

			<div class="top-search-box">
				<a href="https://codecanyon.net/item/nrg-uploader/23667115" class="btn btn-blue" target="_blank">
					<i class="fas fa-download"></i> Download</a>
			</div>

		</div><!--//container-->
	</header><!--//header-->
	<div class="doc-wrapper">
		<div class="container">
			<div id="doc-header" class="doc-header text-center">
				<h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Quick Start</h1>
				<div class="meta"><i class="far fa-clock"></i> Last updated: April 18th, 2019</div>
			</div><!--//doc-header-->
			<div class="doc-body row">
				<div class="doc-content col-md-9 col-12 order-1">
					<div class="content-inner">

						<section id="requirements-section" class="doc-section">
							<h2 class="section-title">Requirements</h2>
							<div class="section-block">
								<ul class="list">
									<li>A web server with PHP support, for example:
										<ul>
											<li><a href="https://httpd.apache.org/" target="_blank">Apache</a> 2.0 or greater</li>
											<li><a href="https://nginx.com/" target="_blank">Nginx</a> 1.1 or greater</li>
										</ul>
									</li>
									<li><a href="https://php.net/" target="_blank">PHP</a> 5.6.x or greater</li>
									<li>PHP extensions are required:
										<ul>
											<li>php-mbstring</li>
										</ul>
									</li>
								</ul>
							</div>
						</section>

						<section id="installation-section" class="doc-section">
							<h2 class="section-title">Installation</h2>

							<div class="section-block">
								<p>
									We will give an example of installation on
									<a href="https://httpd.apache.org/" target="_blank">Apache</a> server on
									<a href="https://www.ubuntu.com/" target="_blank">Ubuntu</a> operating system.
									The default document root for Apache is `/var/www/html` (Ubun14.04 or later).
								</p>

								<div class="code-block">
									<h6>
										Unpack the uploaded archive to any folder on your server with the next command:
									</h6>
									<p><code>unzip nrg-uploader.zip</code></p>
								</div>

								<div class="code-block">
									<h6>After last command you will get two folders:</h6>
									<p>
									<pre>
/dev
/release</pre>
									</p>
								</div>

								<div class="code-block">
									<h6>
										Copy the content of `./release` folder to any folder within web root of the server
										with the next command:
									</h6>
									<p><code>cp -a ./release/ /var/www/html/uploader/</code></p>

									<p>Note: use `sudo` to execute the command above if you face a permissions issue</p>
								</div>

								<div class="code-block">
									<h6>Ultimately you will get the next structure of folders:</h6>
									<p>
									<pre>var/www/html
       /uploader
               /docs
               /server
               /static
               /uploads
               /favicon.ico
               /index.php</pre>
									</p>
								</div>

								<div class="code-block">
									<h6>Now just open an url that's related to the application folder:</h6>
									<p>
									<pre>http://my-site.com/uploader/</pre>
									</p>
								</div>

								<div class="callout-block callout-warning">
									<div class="content">
										<p>
											If necessary, set uploads folder permissions to `755` it's equal to `rwxr-xr-x`
											(see `uploadsFolder` parameter in <a href="#config-section">configuration section</a>)
										</p>
									</div>
								</div>

								<div class="callout-block callout-warning">
									<div class="content">
										<p>
											If you are using <a href="https://nginx.com/" target="_blank">nginx</a>
											server then add following code to server config: <br/>
											`location %application-folder%/server { deny all; return 403; }` <br/>
											It's necessary for server side protection
										</p>
									</div>
								</div>

							</div>
						</section>

						<section id="config-section" class="doc-section">
							<h2 class="section-title">Configuration</h2>
							<div class="section-block">
								<p>The main configuration file location is `%application-folder%/server/config.json`</p>

								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
										<tr>
											<th>Name</th>
											<th>Default</th>
											<th>Description</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td>uploadsFolder</td>
											<td>./uploads</td>
											<td>File upload folder</td>
										</tr>
										<tr>
											<td>maxSize</td>
											<td>1 GB</td>
											<td>
												Maximum allowed uploaded file size.
												Note that php.ini setting has precedence over this setting.
											</td>
										</tr>
										<tr>
											<td>allowExtensions</td>
											<td>null</td>
											<td>
												Array of allowed file extensions.
												If missing or equals to null then all file types are allowed.
												Note that denyExtensions option has precedence over this setting.
											</td>
										</tr>
										<tr>
											<td>denyExtensions</td>
											<td>null</td>
											<td>
												Array of denied file extensions.
												If missing or equals to null then all file types are allowed.
												Note this option has precedence over allowExtensions option.
											</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
						</section>

						<section id="params-section" class="doc-section">
							<h2 class="section-title">Parameters</h2>
							<div class="section-block">
								<p>
									Parameters is a simple object that you pass to the constructor
									of the application class when you create an application instance
									(see <a href="#usage-section">usage section</a>)
								</p>

								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
										<tr>
											<th>Name</th>
											<th>Default</th>
											<th>Description</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td>apiUrl</td>
											<td>.</td>
											<td>URL for the server side of the application</td>
										</tr>
										<tr>
											<td>wrapper</td>
											<td>document.body</td>
											<td>
												Container to render the application is native DOM Element instance.
												You can get it just by the following code:
												<pre>document.getElementById('app')</pre>
											</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
						</section>

						<section id="usage-section" class="doc-section">
							<h2 class="section-title">Usage</h2>

							<div class="section-block" id="usage-embed">
								<div id="html" class="section-block">

									<div class="code-block">
										<h6>Example: how to embed in HTML page</h6>
										<pre class=" language-markup"><code class="language-php">// path: %application-folder%/index.php

&#x3C;?php
if (!empty($_GET[&#x27;r&#x27;])) {
    require __DIR__.&#x27;/server/run.php&#x27;;
    exit(0);
}
?&#x3E;&#x3C;!doctype html&#x3E;
&#x3C;html lang=&#x22;en&#x22;&#x3E;
&#x3C;head&#x3E;
&#x9;&#x3C;title&#x3E;File Uploader&#x3C;/title&#x3E;
&#x9;&#x3C;meta charset=&#x22;utf-8&#x22;/&#x3E;
&#x9;&#x3C;link href=&#x22;./static/css/nrg-uploader.min.css&#x22; rel=&#x22;stylesheet&#x22;&#x3E;
&#x3C;/head&#x3E;
&#x3C;body&#x3E;

&#x3C;div id=&#x22;uploader&#x22;&#x3E;&#x3C;/div&#x3E;

&#x3C;script src=&#x22;./static/js/nrg-uploader.min.js&#x22;&#x3E;&#x3C;/script&#x3E;
&#x3C;script language=&#x22;JavaScript&#x22;&#x3E;
  var uploader = new $nrg.Uploader({
    apiUrl: &#x27;.&#x27;,
    wrapper: document.getElementById(&#x27;uploader&#x27;)
  })

  uploader.run()
&#x3C;/script&#x3E;
&#x3C;/body&#x3E;
&#x3C;/html&#x3E;</code></pre>
									</div>

								</div>
							</div>

							<div class="section-block" id="usage-custom-login">
								<div id="html" class="section-block">

									<div class="code-block">
										<h6>Example: how to use with custom login</h6>
										<pre class=" language-markup"><code class="language-php">// path: %application-folder%/index.php

&#x3C;?php
/**
    Here we check if the user is logged in
    if necessary we send the user to the login page
*/
if (!isset($_SESSION[&#x27;user&#x27;])) {
    header(&#x27;Location: /login&#x27;, true, 302);
    exit();
}

if (!empty($_GET[&#x27;r&#x27;])) {
    require __DIR__.&#x27;/server/run.php&#x27;;
    exit(0);
}
?&#x3E;&#x3C;!doctype html&#x3E;
&#x3C;html lang=&#x22;en&#x22;&#x3E;
&#x3C;head&#x3E;
&#x9;&#x3C;title&#x3E;File Uploader&#x3C;/title&#x3E;
&#x9;&#x3C;meta charset=&#x22;utf-8&#x22;/&#x3E;
&#x9;&#x3C;link href=&#x22;./static/css/nrg-uploader.min.css&#x22; rel=&#x22;stylesheet&#x22;&#x3E;
&#x3C;/head&#x3E;
&#x3C;body&#x3E;

&#x3C;div id=&#x22;uploader&#x22;&#x3E;&#x3C;/div&#x3E;

&#x3C;script src=&#x22;./static/js/nrg-uploader.min.js&#x22;&#x3E;&#x3C;/script&#x3E;
&#x3C;script language=&#x22;JavaScript&#x22;&#x3E;
  var uploader = new $nrg.Uploader({
    apiUrl: &#x27;.&#x27;,
    wrapper: document.getElementById(&#x27;uploader&#x27;)
  })

  uploader.run()
&#x3C;/script&#x3E;
&#x3C;/body&#x3E;
&#x3C;/html&#x3E;</code></pre>
									</div>

								</div>
							</div>

						</section>

						<section id="access-section" class="doc-section">
							<h2 class="section-title">File access</h2>

							<div class="section-block">
								<p>
									By default, after installation, you will have access to uploaded files
									by following url (Replace `{file.png}` to real file name):
								<pre>http://my-site.com/uploader/?r=/open&fileName={file.png}</pre>
								</p>

								<div class="callout-block callout-warning">
									<div class="content">
										<p>
											If you are using <a href="#usage-custom-login">custom login</a> then only logged in
											users can open uploaded files. Otherwise any visitor cans open file by url specified above
										</p>
									</div>
								</div>

								<p>
									If you want to have direct file access that you can just to
									delete file `%application-folder%/uploads/.htaccess` and then open
									file by following url (Replace `{file.png}` to real file name):
								<pre>http://my-site.com/uploader/uploads/{file.png}</pre>
								</p>

								<p>
									If you are going to change the uploads folder location
									then modify `uploadsFolder` parameter from
									`%application-folder%/server/config.json` file
									(see an example below).
								</p>

								<pre class="language-markup"><code class="language-php">// path: %application-folder%/server/config.json

{
  ...
  &#x22;uploadsFolder&#x22;: &#x22;./uploads&#x22;,
  ...
}</code></pre>

							</div>

						</section>

						<section id="development-section" class="doc-section">
							<h2 class="section-title">Development</h2>

							<div class="section-block">
								<p>
									The client part of the application was created with
									<a href="https://github.com/facebook/create-react-app/" target="_blank">Create React App</a>.
									The server part of the application was created with <a href="https://php.net/" target="_blank">PHP</a>.
									So a development new features or modifications requires the next tools to be installed:
								</p>

								<ul class="list">
									<li><a href="https://nodejs.org/" target="_blank">Node</a> 8.10.0 or greater</li>
									<li><a href="https://yarnpkg.com/" target="_blank">Yarn</a> 1.10.0 or greater</li>
									<li><a href="https://php.net/" target="_blank">PHP</a> 5.6.x or greater</li>
									<li>
										<a href="https://httpd.apache.org/" target="_blank">Apache</a> 2.0 or greater or
										<a href="https://nginx.com/" target="_blank">Nginx</a> 1.1 or greater
									</li>
								</ul>

								<div class="code-block">
									<p>The application source code is in the folder `dev` of the downloaded archive:</p>
									<h6>The application has the next structure:</h6>
									<p>
									<pre>
/public
/scripts
/src
/www
/config-overrides.js
/package.json</pre>
									</p>
								</div>

								<div class="code-block">
									<p>The server part of the application is in `www/server` and
										the client part of the application is in 'src'</p>
								</div>

								<div class="code-block">
									<h6>To local deploy the application you can use the next command:</h6>
									<p><code>yarn</code></p>
								</div>

								<div class="code-block">
									<h6>To build the application after making code changes you can use the next command:</h6>
									<p><code>yarn release</code></p>
									<p>After that command is finished all you need to deploy new version of the application will be in
										`www` folder of the project structure</p>
									<p>
										Just put content of `www` folder to the folder where you has earlier
										installed the application on your production web server
										(see <a href="#installation-section">installation section</a>)
									</p>
								</div>

							</div>

						</section>

					</div>
				</div>

				<div class="doc-sidebar col-md-3 col-12 order-0 d-none d-md-flex">
					<div id="doc-nav" class="doc-nav">
						<nav id="doc-menu" class="nav doc-menu flex-column sticky">
							<a class="nav-link scrollto" href="#requirements-section">Requirements</a>
							<a class="nav-link scrollto" href="#installation-section">Installation</a>
							<a class="nav-link scrollto" href="#config-section">Configuration</a>
							<a class="nav-link scrollto" href="#params-section">Parameters</a>
							<a class="nav-link scrollto" href="#usage-section">Usage</a>
							<nav class="doc-sub-menu nav flex-column">
								<a class="nav-link scrollto" href="#usage-embed">Embedding in a page</a>
								<a class="nav-link scrollto" href="#usage-custom-login">Using with custom login</a>
							</nav>
							<a class="nav-link scrollto" href="#access-section">File access</a>
							<a class="nav-link scrollto" href="#development-section">Development</a>
						</nav><!--//doc-menu-->
					</div>
				</div><!--//doc-sidebar-->
			</div><!--//doc-body-->
		</div><!--//container-->
	</div><!--//doc-wrapper-->

<?php
tpl('docs/bottom');
?>