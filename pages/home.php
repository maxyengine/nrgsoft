<?php
$variables = [
    'title' => 'NrgSoft',
    'description' => 'Web software development',
    'keywords' => 'web, software, development',
		'favicon' => "{$root}/assets/main/img/favicon.ico"
];

tpl('main/top', $variables);
?>

	<main class="main-wrapper">

		<!-- Navigation -->
		<header class="nav nav--transparent">

			<div class="nav__holder" id="sticky-nav">
				<div class="container">
					<div class="flex-parent">

						<div class="nav__header clearfix">
							<!-- Logo -->
							<div class="logo-wrap">
								<a href="<?= $root ?>" class="logo__link">
									<span class="logo__name"><i class="ui-skyatlas"></i> NrgSoft</span>
								</a>
							</div>

							<button type="button" class="nav__icon-toggle" id="nav__icon-toggle" data-toggle="collapse"
							        data-target="#navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="nav__icon-toggle-bar"></span>
								<span class="nav__icon-toggle-bar"></span>
								<span class="nav__icon-toggle-bar"></span>
							</button>
						</div> <!-- end nav-header -->

						<nav id="navbar-collapse" class="nav__wrap collapse navbar-collapse">
                <?php tpl('main/nav'); ?>
						</nav> <!-- end nav-wrap -->

					</div> <!-- end flex-parent -->
				</div> <!-- end container -->

			</div>
		</header> <!-- end navigation -->


		<div class="content-wrapper oh">

			<!-- Hero -->
			<section class="hero bg-img bg-gradient white-text"
			         style="background-image: url(<?= $root ?>/assets/main/img/hero/hero_1.jpg);">

				<div class="container  hero__container container-full-height">

					<div class="row hero__outer align-items-center">
						<div class="col-lg-12 text-center">
							<h1>Web software development</h1>
							<div>
								<a href="https://codecanyon.net/user/maxyengine/portfolio" target="_blank"
								   class="btn btn--lg btn--color rounded">
									<span>Portfolio</span>
								</a>
							</div>
						</div>
					</div>
				</div>

			</section> <!-- end hero -->

			<!-- Intro -->
			<section class="section-wrap bottom-divider">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-8 text-center">
							<!-- <span class="intro__subtitle uppercase">About</span> -->
							<h2 class="intro__title">
								We are a young team of talent developers engaged in Software and Web development.
							</h2>
						</div>
					</div>
				</div>
			</section> <!-- end intro -->

			<!-- Newsletter -->
			<section class="section-wrap newsletter bg-img bg-overlay white-text relative"
			         style="background-image: url(<?= $root ?>/assets/main/img/newsletter/newsletter_bg.jpg);">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-6 text-center" style="height: 163px">
						</div>
					</div>
				</div>
			</section> <!-- end Newsletter -->

        <?php tpl('main/footer'); ?>

		</div> <!-- end content wrapper -->
	</main> <!-- end main wrapper -->

<?php tpl('main/bottom'); ?>