<?php
$variables = [
    'title' => 'Uploader | NrgSoft',
    'description' => 'File uploader web application',
    'keywords' => 'file uploader, multiupload',
    'favicon' => "{$root}/assets/main/img/uploader/favicon.ico",
];

tpl('main/top', $variables);
?>

<main class="main-wrapper">

	<!-- Navigation -->
	<header class="nav nav--transparent">

		<div class="nav__holder" id="sticky-nav">
			<div class="container">
				<div class="flex-parent">

					<div class="nav__header clearfix">
						<!-- Logo -->
						<a class="logo" href="<?= $root ?>/products/uploader">
							<svg class="nrgSoftUp__logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51 43" width="32">
								<path fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"
								      d="M13,33A12,12,0,0,1,13,9h.2A10,10,0,0,1,32.31,7.36a7.29,7.29,0,0,1,1.93-.24,7,7,0,0,1,6.82,6.09A10,10,0,0,1,39,33H37"></path>
								<line fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3" x1="25"
								      y1="41" x2="25" y2="21"></line>
								<polyline fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"
								          points="31 27 25 21 19 27"></polyline>
							</svg>
							<span class="logo__name">Uploader</span>
						</a>

						<button type="button" class="nav__icon-toggle" id="nav__icon-toggle" data-toggle="collapse"
						        data-target="#navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="nav__icon-toggle-bar"></span>
							<span class="nav__icon-toggle-bar"></span>
							<span class="nav__icon-toggle-bar"></span>
						</button>
					</div> <!-- end nav-header -->

					<nav id="navbar-collapse" class="nav__wrap collapse navbar-collapse">
              <?php tpl('main/nav'); ?>
					</nav> <!-- end nav-wrap -->

				</div> <!-- end flex-parent -->
			</div> <!-- end container -->

		</div>
	</header> <!-- end navigation -->


	<div class="content-wrapper oh">

		<!-- Hero -->
		<section class="hero bg-img bg-gradient white-text"
		         style="background-image: url(<?= $root ?>/assets/main/img/hero/hero_1.jpg);">

			<div class="container  hero__container container-full-height">

				<div class="row hero__outer align-items-center">
					<div class="col-lg-5">
						<h1 class="hero__title hero--boxed">File upload tool</h1>
						<div class="hero__btn-holder mt-30">
							<a href="<?= $root ?>/demo/uploader" target="_blank" class="btn btn--lg btn--color rounded">
								<span>Try Demo</span>
							</a>
							<a href="https://codecanyon.net/item/nrg-uploader/23667115" target="_blank"
							   class="btn btn--lg btn--light rounded">
								<span>Buy Now</span>
							</a>
						</div>
					</div>
					<div class="col-lg-7 text-right d-none d-lg-block">
						<img src="<?= $root ?>/assets/main/img/uploader/screen-1.png" class="uploader-screen" alt="">
					</div>
				</div>

			</div>
		</section> <!-- end hero -->

		<!-- Partners -->
		<section class="partners text-center bottom-divider">
			<div class="container">
				<div class="row justify-content-center">

					<div class="col col-md-2 col-sm-4 mt-20 mb-20">
						<i class="fab fa-php"></i>
					</div>
					<div class="col col-md-2 col-sm-4 mt-20 mb-20">
						<i class="fab fa-react"></i>
					</div>
					<div class="col col-md-2 col-sm-4 mt-20 mb-20">
						<i class="fab fa-js"></i>
					</div>
					<div class="col col-md-2 col-sm-4 mt-20 mb-20">
						<i class="fab fa-html5"></i>
					</div>
					<div class="col col-md-2 col-sm-4 mt-20 mb-20">
						<i class="fab fa-css3-alt"></i>
					</div>
					<div class="col col-md-2 col-sm-4 mt-20 mb-20">
						<i class="fab fa-sass"></i>
					</div>
				</div>
			</div>
		</section> <!-- end partners -->


		<!-- Intro -->
		<section class="section-wrap bottom-divider">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8 text-center">
						<!-- <span class="intro__subtitle uppercase">About</span> -->
						<h2 class="intro__title">
							It just helps you to upload files on the server via web interface.
							The script was developed on pure PHP and JS and can be deployed easily as separately and can be
							integrated to any CMS as well
						</h2>
					</div>
				</div>
			</div>
		</section> <!-- end intro -->

		<!-- Promo Section -->
		<section class="section-wrap promo-section bottom-divider pb-100">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-7 mb-md-40">
						<img src="<?= $root ?>/assets/main/img/uploader/screen-2.png" alt=""
						     class="promo-section__img uploader-screen">
					</div>
					<div class="col-lg-4 offset-lg-1">
						<div class="promo-section__description">
							<h2 class="promo-section__title">View files after upload</h2>
							<p class="lead">
								The application interface allows the user to easily open the uploaded file in a new browser tab
							</p>
						</div>
					</div>
				</div>
			</div>
		</section> <!-- end promo section -->

		<!-- Promo Section -->
		<section class="section-wrap promo-section bottom-divider pb-100">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-4 mb-md-40">
						<div class="promo-section__description">
							<h2 class="promo-section__title">File validation</h2>
							<p class="lead">
								The application supports two types of validation.
								Client side validation for user convenience and
								server side validation for more safe
							</p>
						</div>
					</div>
					<div class="col-lg-7 offset-lg-1">
						<img src="<?= $root ?>/assets/main/img/uploader/screen-3.png" alt=""
						     class="promo-section__img uploader-screen">
					</div>
				</div>
			</div>
		</section> <!-- end promo section -->

		<!-- Promo Section -->
		<section class="section-wrap promo-section bottom-divider pb-100">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-7 mb-md-40">
						<img src="<?= $root ?>/assets/main/img/uploader/screen-4.png" alt=""
						     class="promo-section__img uploader-screen">
					</div>
					<div class="col-lg-4 offset-lg-1">
						<div class="promo-section__description">
							<h2 class="promo-section__title">Upload retrying</h2>
							<p class="lead">
								The user has the ability to re-upload the file after a server error has occurred
							</p>
						</div>
					</div>
				</div>
			</div>
		</section> <!-- end promo section -->

		<!-- Newsletter -->
		<section class="section-wrap newsletter bg-img bg-overlay white-text relative"
		         style="background-image: url(<?= $root ?>/assets/main/img/newsletter/newsletter_bg.jpg);">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 text-center">
						<h2 class="newsletter__title">Ready to start?</h2>
						<br>
						<div class="hero__btn-holder mt-30">
							<a href="<?= $root ?>/demo/uploader" target="_blank" class="btn btn--lg btn--color rounded">
								<span>Try Demo</span>
							</a>
							<a href="https://codecanyon.net/item/nrg-uploader/23667115" target="_blank"
							   class="btn btn--lg btn--light rounded"><span>Buy Now</span></a>
						</div>
					</div>
				</div>
			</div>
		</section> <!-- end Newsletter -->

      <?php tpl('main/footer'); ?>

	</div> <!-- end content wrapper -->
</main> <!-- end main wrapper -->

<?php tpl('main/bottom'); ?>
