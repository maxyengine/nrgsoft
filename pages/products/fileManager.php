<?php
tpl(
    'main/top',
    [
        'title' => 'File Manager | NrgSoft',
        'description' => 'File manager web application',
        'keywords' => 'file manager, file storage',
        'favicon' => "{$root}/assets/main/img/file-manager/favicon.ico",
    ]
);
?>

<main class="main-wrapper">
	
	<!-- Navigation -->
	<header class="nav nav--transparent">
		
		<div class="nav__holder" id="sticky-nav">
			<div class="container">
				<div class="flex-parent">
					
					<div class="nav__header clearfix">
						<!-- Logo -->
						<div class="logo-wrap">
							<a href="<?= $root ?>/products/fileManager">
								<img class="logo" src="<?= $root ?>/assets/main/img/file-manager/logo.png" alt="logo">
								<span class="logo__name">File Manager</span>
							</a>
						</div>
						
						<button type="button" class="nav__icon-toggle" id="nav__icon-toggle" data-toggle="collapse"
						        data-target="#navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="nav__icon-toggle-bar"></span>
							<span class="nav__icon-toggle-bar"></span>
							<span class="nav__icon-toggle-bar"></span>
						</button>
					</div> <!-- end nav-header -->
					
					<nav id="navbar-collapse" class="nav__wrap collapse navbar-collapse">
              <?php tpl('main/nav'); ?>
					</nav> <!-- end nav-wrap -->
				
				</div> <!-- end flex-parent -->
			</div> <!-- end container -->
		
		</div>
	</header> <!-- end navigation -->
	
	
	<div class="content-wrapper oh">
		
		<!-- Hero -->
		<section class="hero bg-img bg-gradient white-text"
		         style="background-image: url(<?= $root ?>/assets/main/img/hero/hero_1.jpg);">
			
			<div class="container  hero__container container-full-height">
				
				<div class="row hero__outer align-items-center">
					<div class="col-lg-5">
						<h1 class="hero__title hero--boxed">Perfect solution <br class='visible-lg'>for managing your files</h1>
						<div class="hero__btn-holder mt-30">
							<a href="<?= $root ?>/demo/fileManager" target="_blank" class="btn btn--lg btn--color rounded">
								<span>Try Demo</span>
							</a>
							<a href="https://codecanyon.net/item/engine-file-manager/20829537" target="_blank"
							   class="btn btn--lg btn--light rounded"><span>Buy Now</span></a>
						</div>
					</div>
					<div class="col-lg-7 text-right d-none d-lg-block">
						<a href="<?= $root ?>/assets/main/img/file-manager/screen-1.jpg" data-fancybox>
							<img src="<?= $root ?>/assets/main/img/file-manager/screen-1.jpg" class="file-manager-screen" alt="">
						</a>
					</div>
				</div>
			
			</div>
		</section> <!-- end hero -->
		
		<!-- Partners -->
		<section class="partners text-center bottom-divider">
			<div class="container">
				<div class="row justify-content-center">
					
					<div class="col-lg-2 col-sm-4 mt-20 mb-20">
						<i class="fab fa-php"></i>
					</div>
					<div class="col-lg-2 col-sm-4 mt-20 mb-20">
						<i class="fab fa-js"></i>
					</div>
					<div class="col-lg-2 col-sm-4 mt-20 mb-20">
						<i class="fab fa-html5"></i>
					</div>
					<div class="col-lg-2 col-sm-4 mt-20 mb-20">
						<i class="fab fa-css3-alt"></i>
					</div>
					<div class="col-lg-2 col-sm-4 mt-20 mb-20">
						<i class="fab fa-sass"></i>
					</div>
				</div>
			</div>
		</section> <!-- end partners -->
		
		
		<!-- Intro -->
		<section class="section-wrap bottom-divider">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8 text-center">
						<!-- <span class="intro__subtitle uppercase">About</span> -->
						<h2 class="intro__title">
							An idea of the application comes from desktop analogs. It's real convenient way to managing your files,
							folders and hyperlinks on the server via web interface.
							The script was developed on pure PHP and JS and can be deployed easily as separately and can be
							integrated to any CMS as well
						</h2>
					</div>
				</div>
			</div>
		</section> <!-- end intro -->
		
		<!-- Promo Section -->
		<section class="section-wrap promo-section bottom-divider pb-100">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 mb-md-40">
						<a href="<?= $root ?>/assets/main/img/file-manager/screen-2.jpg" data-fancybox>
							<img src="<?= $root ?>/assets/main/img/file-manager/screen-2.jpg" alt=""
							     class="promo-section__img file-manager-screen">
						</a>
					</div>
					<div class="col-lg-5 offset-lg-1">
						<div class="promo-section__description">
							<h2 class="promo-section__title">Intuitive user interface</h2>
							<p class="lead">The application interface allows the user to easily manage their files with the
								comfortable and
								intuitive approach already known from the daily desktop working environment and minimize user learning
								curve</p>
						</div>
					</div>
				</div>
			</div>
		</section> <!-- end promo section -->
		
		<!-- Promo Section -->
		<section class="section-wrap promo-section bottom-divider pb-100">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-5 mb-md-40">
						<div class="promo-section__description">
							<h2 class="promo-section__title">Advanced uploads</h2>
							<p class="lead">
								Upload multiple files at once.
								User cans break uploading for each file separately.
								User specifies a file size limit.
								User specifies allowed files types
							</p>
						</div>
					</div>
					<div class="col-lg-6 offset-lg-1">
						<a href="<?= $root ?>/assets/main/img/file-manager/screen-4.jpg" data-fancybox>
							<img src="<?= $root ?>/assets/main/img/file-manager/screen-4.jpg" alt=""
							     class="promo-section__img file-manager-screen">
						</a>
					</div>
				</div>
			</div>
		</section> <!-- end promo section -->
		
		
		<!-- Newsletter -->
		<section class="section-wrap newsletter bg-img bg-overlay white-text relative"
		         style="background-image: url(<?= $root ?>/assets/main/img/newsletter/newsletter_bg.jpg);">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 text-center">
						<h2 class="newsletter__title">Ready to start?</h2>
						<br>
						<div class="hero__btn-holder mt-30">
							<a href="<?= $root ?>/demo/fileManager" target="_blank" class="btn btn--lg btn--color rounded">
								<span>Try Demo</span>
							</a>
							<a href="https://codecanyon.net/item/engine-file-manager/20829537" target="_blank"
							   class="btn btn--lg btn--light rounded">
								<span>Buy Now</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</section> <!-- end Newsletter -->

      <?php tpl('main/footer'); ?>
	
	</div> <!-- end content wrapper -->
</main> <!-- end main wrapper -->

<?php tpl('main/bottom'); ?>
