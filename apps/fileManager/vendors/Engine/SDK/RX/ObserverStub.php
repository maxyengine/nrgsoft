<?php

namespace Engine\SDK\RX;

use Throwable;

trait ObserverStub
{
    public function onNext($event)
    {
    }

    public function onError(Throwable $throwable, $event)
    {
    }

    public function onComplete()
    {
    }
}
