<?php

namespace Engine\SDK\RX\Contract;

use Throwable;

interface Observer
{
    public function onNext($event);

    public function onError(Throwable $throwable, $event);

    public function onComplete();
}
