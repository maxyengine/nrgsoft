<?php

namespace Engine\SDK\RX;

use Engine\SDK\RX\Contract\Observer;
use SplObjectStorage;
use Throwable;

class Observable
{
    /**
     * @var Observer[]
     */
    private $storage;

    public function __construct()
    {
        $this->storage = new SplObjectStorage();
    }

    public function addObserver(Observer $observer)
    {
        $this->storage->attach($observer);

        return $this;
    }

    public function removeObserver(Observer $observer)
    {
        $this->storage->detach($observer);

        return $this;
    }

    public function notifyObservers($event)
    {
        try {
            foreach ($this->storage as $observer) {
                $observer->onNext($event);
            }
            foreach ($this->storage as $observer) {
                $observer->onComplete();
            }
        } catch (Throwable $throwable) {
            foreach ($this->storage as $observer) {
                $observer->onError($throwable, $event);
            }
        }
    }
}
