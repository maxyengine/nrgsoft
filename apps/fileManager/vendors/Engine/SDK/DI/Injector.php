<?php

namespace Engine\SDK\DI;

use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;
use ReflectionParameter;

/**
 * Class Injector
 * @package Engine\SDK\DI
 */
class Injector
{
    /**
     * @var array
     */
    private $services = [];

    public function createObject($class, array $args = [])
    {
        $class = new ReflectionClass($class);

        return $class->hasMethod('__construct') ?
            $class->newInstanceArgs($this->resolveArguments($class->getMethod('__construct')->getParameters(), $args)) :
            $class->newInstance();
    }

    public function invokeMethod($object, $name, array $args = [])
    {
        $method = new ReflectionMethod($object, $name);

        return $method->invokeArgs($object, $this->resolveArguments($method->getParameters(), $args));
    }

    public function invokeFunction(callable $function, array $args = [])
    {
        $function = new ReflectionFunction($function);

        return $function->invokeArgs($this->resolveArguments($function->getParameters(), $args));
    }

    public function setService($interface, $definition)
    {
        $this->services[$interface] = $definition;

        return $this;
    }

    public function loadServices(array $definitions)
    {
        foreach ($definitions as $interface => $definition) {
            $this->setService($interface, $definition);
        }

        return $this;
    }

    private function getService($interface)
    {
        if (isset($this->services[$interface])) {
            return $this->createService($interface);
        }

        foreach ($this->services as $serviceInterface => $definition) {
            $service = new ReflectionClass($serviceInterface);
            if ($service->isSubclassOf($interface)) {
                $this->setService($interface, $this->createService($serviceInterface));
                return $this->services[$interface];
            }
        }

        return null;
    }

    private function createService($interface)
    {
        $definition = $this->services[$interface];

        if (!is_object($definition) || is_callable($definition)) {
            if (is_array($definition)) {
                $this->services[$interface] = $this->createObject(array_shift($definition), $definition);
            } elseif (is_callable($definition)) {
                $callable = new ReflectionFunction($definition);
                $this->services[$interface] = $callable->invokeArgs($this->resolveArguments($callable->getParameters()));
            } else {
                $this->services[$interface] = $this->createObject($definition);
            }
        }

        return $this->services[$interface];
    }

    /**
     * @param ReflectionParameter[] $parameterReflections
     * @param array $passed
     *
     * @return array
     */
    private function resolveArguments(array $parameterReflections, array $passed = [])
    {
        $arguments = [];
        foreach ($parameterReflections as $param) {
            if (isset($passed[$param->name])) {
                $arguments[] = $passed[$param->name];
            } elseif (null !== $param->getClass()) {
                $class = $param->getClass()->name;
                $service = $this->getService($class);
                if (null !== $service) {
                    $arguments[] = $service;
                } elseif ($param->isDefaultValueAvailable()) {
                    $arguments[] = $param->getDefaultValue();
                } else {
                    $arguments[] = $this->createObject($class);
                }
            } elseif ($param->isDefaultValueAvailable()) {
                $arguments[] = $param->getDefaultValue();
            }
        }

        return $arguments;
    }
}
