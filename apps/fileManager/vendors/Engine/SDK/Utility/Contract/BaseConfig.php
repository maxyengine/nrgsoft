<?php

namespace Engine\SDK\Utility\Contract;

/**
 * Interface BaseConfig
 * @package Engine\SDK\Addition\Contract
 */
interface BaseConfig
{
    /**
     * @param $key
     * @param null $defaultValue
     *
     * @return mixed
     */
    public function get($key, $defaultValue = null);

    /**
     * @param $key
     * @param $value
     *
     * @return mixed
     */
    public function set($key, $value);

    /**
     * @return array
     */
    public function asArray(): array;
}
