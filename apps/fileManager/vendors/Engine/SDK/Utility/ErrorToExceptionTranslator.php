<?php

namespace Engine\SDK\Utility;

use ErrorException;

/**
 * Class ErrorToExceptionTranslator
 * @package Engine\SDK\Utility
 */
class ErrorToExceptionTranslator
{
    /**
     * @var int
     */
    private $errorTypes;

    /**
     * @var bool
     */
    private $ignoreErrorReporting;

    /**
     * @var bool
     */
    private $ignoreControlOperator;

    /**
     * @var bool
     */
    private $enabled = false;

    /**
     * ErrorToExceptionTranslator constructor.
     *
     * @param int $errorTypes
     * @param bool $ignoreErrorReporting
     * @param bool $ignoreControlOperator
     */
    public function __construct(
        $errorTypes = E_ALL | E_STRICT,
        $ignoreErrorReporting = false,
        $ignoreControlOperator = false
    ) {
        $this->errorTypes = $errorTypes;
        $this->ignoreErrorReporting = $ignoreErrorReporting;
        $this->ignoreControlOperator = $ignoreControlOperator;
    }

    public function enable()
    {
        set_error_handler(function ($code, $message, $file, $line) {
            return $this->errorHandler($code, $message, $file, $line);
        }, $this->errorTypes);

        register_shutdown_function(function () {
            if ($this->enabled) {
                $this->shutdownHandler();
            }
        });

        $this->enabled = true;
    }

    public function disable()
    {
        set_error_handler(null);
        $this->enabled = false;
    }

    /**
     * @param $code
     * @param $message
     * @param $file
     * @param $line
     *
     * @return bool
     * @throws ErrorException
     */
    private function errorHandler($code, $message, $file, $line): bool
    {
        if (!$this->ignoreErrorReporting) {
            if (!$this->ignoreControlOperator && 0 === error_reporting()) {
                return false;
            }
            if (!(error_reporting() & $code)) {
                return false;
            }
        }

        throw new ErrorException($message, $code, 1, $file, $line);
    }

    private function shutdownHandler()
    {
        $error = error_get_last();
        if (null !== $error) {
            $this->errorHandler($error["type"], $error["message"], $error["file"], $error["line"]);
        }
    }
}
