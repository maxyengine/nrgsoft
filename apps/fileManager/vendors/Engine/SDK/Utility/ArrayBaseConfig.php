<?php

namespace Engine\SDK\Utility;

use Engine\SDK\Utility\Contract\BaseConfig;

class ArrayBaseConfig implements BaseConfig
{
    /**
     * @var array
     */
    private $values;

    public function __construct(array $values)
    {
        $this->values = $values;
    }

    /**
     * {@inheritdoc}
     */
    public function get($key, $defaultValue = null)
    {
        return $this->values[$key] ?? $defaultValue;
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value)
    {
        $this->values[$key] = $value;
    }

    /**
     * @return array
     */
    public function asArray(): array
    {
        return $this->values;
    }
}
