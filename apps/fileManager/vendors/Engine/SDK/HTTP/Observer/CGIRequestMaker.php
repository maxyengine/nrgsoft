<?php

namespace Engine\SDK\HTTP\Observer;

use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\HTTP\Service\MakeCGIRequest;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

class CGIRequestMaker implements Observer
{
    use ObserverStub;

    /**
     * @var MakeCGIRequest
     */
    private $makeCGIRequest;

    /**
     * CGIRequestMaker constructor.
     */
    public function __construct()
    {
        $this->makeCGIRequest = new MakeCGIRequest();
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->makeCGIRequest->execute($event->getRequest());
    }
}
