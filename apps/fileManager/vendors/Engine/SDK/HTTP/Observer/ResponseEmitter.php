<?php

namespace Engine\SDK\HTTP\Observer;

use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\HTTP\Service\EmitResponse;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;
use Throwable;

class ResponseEmitter implements Observer
{
    use ObserverStub;

    /**
     * @var EmitResponse
     */
    private $emitResponse;

    /**
     * CGIRequestMaker constructor.
     */
    public function __construct()
    {
        $this->emitResponse = new EmitResponse();
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->emitResponse->execute($event->getResponse());
    }

    /**
     * @param Throwable $throwable
     * @param ExchangeEvent $event
     */
    public function onError(Throwable $throwable, $event)
    {
        $this->emitResponse->execute($event->getResponse());
    }
}
