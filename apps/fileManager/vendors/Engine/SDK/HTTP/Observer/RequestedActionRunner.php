<?php

namespace Engine\SDK\HTTP\Observer;

use Engine\SDK\DI\Injector;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\HTTP\Service\CreateRequestedAction;
use Engine\SDK\RX\Contract\Observer;
use Throwable;

class RequestedActionRunner implements Observer
{
    /**
     * @var CreateRequestedAction
     */
    private $createRequestedAction;

    /**
     * @var Observer|null
     */
    private $action;

    /**
     * RequestedActionRunner constructor.
     *
     * @param Injector $injector
     * @param array $routes
     */
    public function __construct(Injector $injector, array $routes)
    {
        $this->createRequestedAction = new CreateRequestedAction($injector, $routes);
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->action = $this->createRequestedAction->execute($event->getRequest());
        $this->action->onNext($event);
    }

    public function onError(Throwable $throwable, $event)
    {
        if (null !== $this->action) {
            $this->action->onError($throwable, $event);
        }
    }

    public function onComplete()
    {
        if (null !== $this->action) {
            $this->action->onComplete();
        }
    }
}
