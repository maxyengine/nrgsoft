<?php

namespace Engine\SDK\HTTP\Service;

use Engine\SDK\HTTP\Value\CGIUrl;
use Engine\SDK\HTTP\Value\Request;
use Engine\SDK\HTTP\Value\UploadedFile;

class MakeCGIRequest
{
    /**
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $request
            ->setProtocolVersion(substr($_SERVER['SERVER_PROTOCOL'], strpos($_SERVER['SERVER_PROTOCOL'], "/") + 1))
            ->setUrl(new CGIUrl())
            ->setMethod($_SERVER['REQUEST_METHOD'])
            ->setCookies($_COOKIE)
            ->setQueryParams($_GET)
            ->setBody(file_get_contents('php://input'))
            ->setBodyParams($_POST)
            ->setUploadedFiles($this->convertToUploadedFiles($_FILES));

        foreach (getallheaders() as $name => $value) {
            $request->setHeader($name, $value);
        }
    }

    /**
     * @param array $files
     *
     * @return array
     */
    private function convertToUploadedFiles(array $files)
    {
        $uploadedFiles = [];
        foreach ($files as $key => $file) {
            $uploadedFiles[$key] = new UploadedFile(
                $file['name'],
                $file['type'],
                $file['tmp_name'],
                $file['error'],
                $file['size']
            );;
        }

        return $uploadedFiles;
    }
}
