<?php

namespace Engine\SDK\HTTP\Service;

use Engine\SDK\HTTP\Value\Response;

class EmitResponse
{
    public function execute(Response $response)
    {
        header(sprintf('HTTP/%s %d %s',
            $response->getProtocolVersion(),
            $response->getStatus()->getCode(),
            $response->getStatus()->getReasonPhrase()
        ));

        foreach ($response->getHeaders() as $name => $values) {
            $name = str_replace(' ', '-', ucwords(str_replace('-', ' ', $name)));;
            foreach ($values as $value) {
                header(sprintf('%s: %s', $name, $value), false);
            }
        }

        echo $response->getBody();
    }
}
