<?php

namespace Engine\SDK\HTTP\Service;

use Engine\SDK\DI\Injector;
use Engine\SDK\HTTP\Value\Request;
use Engine\SDK\RX\Contract\Observer;

class CreateRequestedAction
{
    /**
     * @var Injector
     */
    private $injector;

    /**
     * @var array
     */
    private $routes;

    /**
     * RunRequestedAction constructor.
     *
     * @param Injector $injector
     * @param array $routes
     */
    public function __construct(Injector $injector, array $routes)
    {
        $this->injector = $injector;
        $this->routes = $routes;
    }

    /**
     * @param Request $request
     *
     * @return Observer
     */
    public function execute(Request $request): Observer
    {
        $class = $this->routes[$request->getMethod() . ':' . $request->getUrl()->getPath()] ??
            $this->routes[$request->getUrl()->getPath()] ??
            $this->routes[null];

        $action = $this->injector->createObject($class);

        /**
         * @var Observer $action
         */
        return $action;
    }
}
