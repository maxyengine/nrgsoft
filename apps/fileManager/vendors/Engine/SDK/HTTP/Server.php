<?php

namespace Engine\SDK\HTTP;

use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\HTTP\Value\Request;
use Engine\SDK\HTTP\Value\Response;
use Engine\SDK\RX\Observable;

/**
 * Class Server
 * @package Engine\SDK\HTTP
 */
class Server
{
    /**
     * @param Request $request
     * @param Response $response
     * @param Observable $eventResource
     */
    public function exchange(Request $request, Response $response, Observable $eventResource)
    {
        $eventResource->notifyObservers(new ExchangeEvent($request, $response));
    }
}
