<?php

namespace Engine\SDK\HTTP\Value;

use InvalidArgumentException;

class Url
{
    private $scheme;

    private $user;

    private $password;

    private $host;

    private $port;

    private $basePath;

    private $path;

    private $query;

    private $fragment;

    private $asString;

    public function __construct($url, $basePath = null)
    {
        $partials = parse_url($url);
        $basePath = trim($basePath, '/');

        $this->scheme = isset($partials['scheme']) ? $partials['scheme'] : null;
        $this->user = isset($partials['user']) ? $partials['user'] : null;
        $this->password = isset($partials['pass']) ? $partials['pass'] : null;
        $this->host = isset($partials['host']) ? $partials['host'] : null;
        $this->port = isset($partials['port']) ? $partials['port'] : null;
        $this->query = isset($partials['query']) ? $partials['query'] : null;
        $this->fragment = isset($partials['fragment']) ? $partials['fragment'] : null;

        if ($basePath) {
            $this->basePath = '/' . $basePath;
            if (isset($partials['path'])) {
                $path = '/' . trim($partials['path'], '/');
                if (
                    substr($path, 0, strlen($this->basePath)) !== $this->basePath ||
                    isset($path[strlen($this->basePath)]) && '/' !== $path[strlen($this->basePath)]
                ) {
                    throw new InvalidArgumentException('Invalid basePath, it must be part of path');
                }
                $path = substr($path, strlen($this->basePath));
                $this->path = '/' . trim($path, '/');
            } else {
                $this->path = '/';
            }
        } else {
            $this->path = isset($partials['path']) ? '/' . trim($partials['path'], '/') : '/';
        }
    }

    /**
     * @return null
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * @param string $scheme
     *
     * @return $this
     */
    public function setScheme($scheme)
    {
        $this->scheme = $scheme;
        $this->asString = null;

        return $this;
    }

    /**
     * @return null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        $this->asString = null;

        return $this;
    }

    /**
     * @return null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        $this->asString = null;

        return $this;
    }

    /**
     * @return null
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     *
     * @return $this
     */
    public function setHost($host)
    {
        $this->host = $host;
        $this->asString = null;

        return $this;
    }

    /**
     * @return null
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $port
     *
     * @return $this
     */
    public function setPort($port)
    {
        $this->port = $port;
        $this->asString = null;

        return $this;
    }

    /**
     * @param string $basePath
     *
     * @return self
     */
    public function setBasePath($basePath)
    {
        $this->basePath = '/' . trim($basePath, '/');
        $this->asString = null;

        return $this;
    }

    /**
     * @return string
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return Url
     */
    public function setPath($path)
    {
        $this->path = '/' . trim($path, '/');
        $this->asString = null;

        return $this;
    }

    /**
     * @return null
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param string $query
     *
     * @return $this
     */
    public function setQuery($query)
    {
        $this->query = $query;
        $this->asString = null;

        return $this;
    }

    /**
     * @return array
     */
    public function getQueryParams()
    {
        if (null === $this->query) {
            return [];
        }

        parse_str($this->query, $params);

        return $params;
    }

    /**
     * @param array $params
     *
     * @return $this
     */
    public function setQueryParams(array $params)
    {
        $this->query = empty($params) ? null : http_build_query($params);
        $this->asString = null;

        return $this;
    }

    /**
     * @return null
     */
    public function getFragment()
    {
        return $this->fragment;
    }

    /**
     * @param string $fragment
     *
     * @return $this
     */
    public function setFragment($fragment)
    {
        $this->fragment = $fragment;
        $this->asString = null;

        return $this;
    }

    public function getValue(): string
    {
        return (string)$this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (null !== $this->asString) {
            return $this->asString;
        }

        $this->asString = '';

        if (null !== $this->scheme) {
            $this->asString .= $this->scheme . '://';
        }
        if (null !== $this->user) {
            $this->asString .= $this->user;
        }
        if (null !== $this->password && null !== $this->user) {
            $this->asString .= ':' . $this->password;
        }
        if (null !== $this->host) {
            $this->asString .= null === $this->user ? $this->host : '@' . $this->host;
        }
        if (null !== $this->port && null !== $this->host) {
            $this->asString .= ':' . $this->port;
        }
        $this->asString .= rtrim($this->basePath, '/');
        $this->asString .= $this->path;
        if (null !== $this->query) {
            $this->asString .= '?' . $this->query;
        }
        if (null !== $this->fragment) {
            $this->asString .= '#' . $this->fragment;
        }

        return $this->asString;
    }
}
