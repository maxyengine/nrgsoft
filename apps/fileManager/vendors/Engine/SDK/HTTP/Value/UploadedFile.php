<?php

namespace Engine\SDK\HTTP\Value;

use RuntimeException;
use InvalidArgumentException;

class UploadedFile
{
    /**
     * @var null|string
     */
    private $name;

    /**
     * @var null|string
     */
    private $type;

    /**
     * @var null|string
     */
    private $tmpName;

    /**
     * @var null|integer
     */
    private $error;

    /**
     * @var null|integer
     */
    private $size;

    /**
     * @var bool
     */
    private $moved = false;

    public function __construct(string $name, string $type, string $tmpName, int $error, int $size)
    {
        $this->name = $name;
        $this->type = $type;
        $this->tmpName = $tmpName;
        $this->error = $error;
        $this->size = $size;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function getError(): int
    {
        return $this->error;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function moveTo(string $targetPath)
    {
        if (UPLOAD_ERR_OK !== $this->error) {
            throw new RuntimeException('Cannot move file, the file has been loaded with an error');
        }
        if (empty($targetPath)) {
            throw new InvalidArgumentException('Invalid targetPath provided, must be a non-empty string');
        }
        if ($this->moved) {
            throw new RuntimeException('Cannot move file after it has already been moved');
        }
        if (empty(PHP_SAPI) || strpos(PHP_SAPI, 'cli') === 0) {
            rename($this->tmpName, $targetPath);
        } else {
            if (!is_uploaded_file($this->tmpName)) {
                throw new RuntimeException('Cannot move file it was not uploaded via HTTP POST');
            }
            if (move_uploaded_file($this->tmpName, $targetPath) === false) {
                throw new RuntimeException('Error occurred during move operation');
            }
        }
        $this->moved = true;
    }
}
