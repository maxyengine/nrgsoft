<?php

namespace Engine\SDK\HTTP\Value;

class CGIUrl extends Url
{
    public function __construct()
    {
        $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $basePath = substr($_SERVER['REQUEST_URI'], 0,
            strrpos($_SERVER['SCRIPT_FILENAME'], '/') - strlen($_SERVER['DOCUMENT_ROOT'])
        );

        parent::__construct($url, $basePath);
    }
}
