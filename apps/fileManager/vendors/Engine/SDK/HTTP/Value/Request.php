<?php

namespace Engine\SDK\HTTP\Value;

class Request
{
    use MessageTrait;

    /**
     * @var Url
     */
    private $url;

    /**
     * @var string
     */
    private $method;

    /**
     * @var array
     */
    private $cookies = [];

    /**
     * @var array
     */
    private $queryParams = [];

    /**
     * @var mixed|null
     */
    private $bodyParams;

    /**
     * @var array
     */
    private $uploadedFiles = [];

    /**
     * @return Url
     */
    public function getUrl(): Url
    {
        return $this->url;
    }

    /**
     * @param Url $url
     *
     * @return $this
     */
    public function setUrl(Url $url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     *
     * @return $this
     */
    public function setMethod(string $method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return array
     */
    public function getCookies(): array
    {
        return $this->cookies;
    }

    /**
     * @param array $cookies
     *
     * @return $this
     */
    public function setCookies(array $cookies)
    {
        $this->cookies = $cookies;

        return $this;
    }

    /**
     * @return array
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * @param $name
     * @param null $default
     *
     * @return mixed|null
     */
    public function getQueryParam($name, $default = null)
    {
        return $this->queryParams[$name] ?? $default;
    }

    /**
     * @param array $queryParams
     *
     * @return $this
     */
    public function setQueryParams(array $queryParams)
    {
        $this->queryParams = $queryParams;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBodyParams()
    {
        return $this->bodyParams;
    }

    /**
     * @param mixed $bodyParams
     *
     * @return $this
     */
    public function setBodyParams($bodyParams)
    {
        $this->bodyParams = $bodyParams;

        return $this;
    }

    /**
     * @param $name
     * @param mixed|null $default
     *
     * @return null|string
     */
    public function getBodyParam($name, $default = null)
    {
        return $this->bodyParams[$name] ?? $default;
    }

    /**
     * @return UploadedFile[]
     */
    public function getUploadedFiles(): array
    {
        return $this->uploadedFiles;
    }

    /**
     * @param UploadedFile[] $uploadedFiles
     *
     * @return $this
     */
    public function setUploadedFiles(array $uploadedFiles)
    {
        $this->uploadedFiles = $uploadedFiles;

        return $this;
    }
}
