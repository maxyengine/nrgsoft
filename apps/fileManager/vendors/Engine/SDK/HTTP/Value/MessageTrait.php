<?php

namespace Engine\SDK\HTTP\Value;

trait MessageTrait
{
    /**
     * HTTP protocol version.
     *
     * @var string
     */
    private $protocolVersion = '1.1';

    /**
     * HTTP headers.
     *
     * @var array
     */
    private $headers = [];

    /**
     * HTTP headers original names.
     *
     * @var array
     */
    private $headerOriginalNames = [];

    /**
     * HTTP message body.
     *
     * @var null|mixed
     */
    private $body;

    public function getProtocolVersion()
    {
        return $this->protocolVersion;
    }

    public function setProtocolVersion($version)
    {
        $this->protocolVersion = $version;

        return $this;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function hasHeader($name)
    {
        return array_key_exists(strtolower($name), $this->headerOriginalNames);
    }

    public function getHeader($name)
    {
        if (!$this->hasHeader($name)) {
            return [];
        }

        $originalName = $this->headerOriginalNames[strtolower($name)];

        return $this->headers[$originalName];
    }

    public function getHeaderLine($name)
    {
        $header = $this->getHeader($name);

        return empty($header) ? '' : implode(',', $header);
    }

    public function setHeader($name, $value)
    {
        $this->unsetHeader($name);

        if (!is_array($value)) {
            $value = [(string)$value];
        }
        $this->headerOriginalNames[strtolower($name)] = $name;
        $this->headers[$name] = $value;

        return $this;
    }

    public function setAddedHeader($name, $value)
    {
        if ($this->hasHeader($name)) {
            if (is_string($value)) {
                $value = [$value];
            }
            $this->setHeader($name, array_merge($this->getHeader($name), $value));
        } else {
            $this->setHeader($name, $value);
        }

        return $this;
    }

    public function unsetHeader($name)
    {
        if ($this->hasHeader($name)) {
            $lowCaseName = strtolower($name);
            $originalName = $this->headerOriginalNames[$lowCaseName];
            unset($this->headers[$originalName], $this->headerOriginalNames[$lowCaseName]);
        }

        return $this;
    }

    public function containsInHeader($name, $needle)
    {
        foreach ($this->getHeader($name) as $value) {
            if (strpos($value, $needle) !== false) {
                return true;
            }
        }

        return false;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }
}
