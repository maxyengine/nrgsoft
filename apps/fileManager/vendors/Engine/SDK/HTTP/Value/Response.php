<?php

namespace Engine\SDK\HTTP\Value;

class Response
{
    use MessageTrait;

    /**
     * @var Status
     */
    private $status;

    public function getStatus(): Status
    {
        return $this->status ?? new Status(200);
    }

    /**
     * @param Status $status
     *
     * @return Response
     */
    public function setStatus(Status $status): Response
    {
        $this->status = $status;

        return $this;
    }
}
