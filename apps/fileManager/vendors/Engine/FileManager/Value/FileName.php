<?php

namespace Engine\FileManager\Value;

use InvalidArgumentException;

/**
 * Class FileName
 * @package Engine\FileManager\Value
 */
class FileName
{
    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $baseName;

    /**
     * @var string
     */
    private $extension;

    /**
     * FileName constructor.
     *
     * @param string $value
     * @param bool $isDirectory
     */
    public function __construct(string $value, bool $isDirectory = false)
    {
        $this->setValue($value, $isDirectory);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getBaseName(): string
    {
        return $this->baseName;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function isEqual(string $id): bool
    {
        return $this->getValue() === $id;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getValue();
    }

    /**
     * @param string $value
     *
     * @return bool
     */
    private function isValid(string $value): bool
    {
        return !preg_match('/(\/|\\\\|\*|\?|\"|>|<|\|)/iu', $value);
    }

    /**
     * @param string $value
     * @param bool $isDirectory
     */
    private function setValue(string $value, bool $isDirectory)
    {
        if (!$this->isValid($value)) {
            throw new InvalidArgumentException('Invalid file name was provided');
        }

        $this->value = $value;

        $extension = '';
        $baseName = $value;

        if (!$isDirectory) {
            $position = mb_strrpos($value, '.');
            $extension = false === $position ? '' : (string)mb_substr($value, $position + 1);
            if ($extension) {
                $baseName = substr($baseName, 0, -strlen($extension) - 1);
            }
        }

        $this->extension = $extension;
        $this->baseName = $baseName;
    }
}
