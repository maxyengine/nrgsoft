<?php

namespace Engine\FileManager\Value;

use InvalidArgumentException;

/**
 * Class FileId
 * @package Engine\FileManager\Value
 */
class FileId
{
    /**
     * @var string
     */
    private $value;

    /**
     * @var FileId|null
     */
    private $parent;

    /**
     * FileId constructor.
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->setValue($value);
        $this->createParent();
    }

    public static function createFromPath(string $path)
    {
        return new self('/' . ltrim(str_replace(DIRECTORY_SEPARATOR, '/', $path)), '/');
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    public function hasParent(): bool
    {
        return null !== $this->parent;
    }

    public function getParent(): FileId
    {
        return $this->parent;
    }

    /**
     * @param string $value
     *
     * @return FileId
     */
    public function join(string $value): FileId
    {
        return new self(rtrim($this->getValue(), '/') . '/' . ltrim($value, '/'));
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function isEqual(string $id): bool
    {
        return $this->getValue() === $id;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getValue();
    }

    /**
     * @param string $value
     *
     * @return bool
     */
    private function isValid(string $value): bool
    {
        return '/' === $value ||
            (
                preg_match('/^\/[^*?"|><\\\\]*[^*?"|><\/\\\\]$/iu', $value) &&
                !preg_match('/\/\.{1,2}$/iu', $value) &&
                !preg_match('/^\/\//iu', $value) &&
                !preg_match('/\/\.\.\//iu', $value)
            );
    }

    /**
     * @param string $value
     */
    private function setValue(string $value)
    {
        if (!$this->isValid($value)) {
            throw new InvalidArgumentException('Invalid file id was provided');
        }

        $this->value = $value;
    }

    private function createParent()
    {
        if (!$this->isEqual('/')) {
            $this->parent = new self(
                '/' . ltrim(mb_substr($this->getValue(), 0, mb_strrpos($this->getValue(), '/')), '/')
            );
        }
    }
}
