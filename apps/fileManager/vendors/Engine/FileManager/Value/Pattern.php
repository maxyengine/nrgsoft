<?php

namespace Engine\FileManager\Value;

class Pattern
{
    /**
     * @var string
     */
    private $value;

    public function __construct(string $value)
    {
        $this->setValue($value);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    public function __toString()
    {
        return (string)$this->getValue();
    }
}
