<?php

namespace Engine\FileManager\Value;

use InvalidArgumentException;

class Permissions
{
    /**
     * @var int
     */
    private $value;

    public function __construct(int $value)
    {
        $this->setValue($value);
    }

    public static function createOfOctalString(string $value)
    {
        return new self(octdec($value));
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    public function __toString()
    {
        return '0' . (string)decoct($this->getValue() & 0777);
    }

    private function isValid(int $value)
    {
        return $value >= 0 && $value <= 0777;
    }

    /**
     * @param int $value
     */
    private function setValue(int $value)
    {
        if (!$this->isValid($value)) {
            throw new InvalidArgumentException('Invalid permissions were provided. They must be in range 0..0777');
        }

        $this->value = $value;
    }
}
