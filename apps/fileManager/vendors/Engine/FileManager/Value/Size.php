<?php

namespace Engine\FileManager\Value;

class Size
{
    /**
     * @var int
     */
    private $value;

    public function __construct(int $value)
    {
        $this->setValue($value);
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue(int $value)
    {
        $this->value = $value;
    }

    public function __toString()
    {
        return (string)$this->getValue();
    }

    /**
     * @return string
     */
    public function toHumanString(): string
    {
        $bytes = $this->getValue();
        $thresh = 1024;

        if (abs($bytes) < $thresh) {
            return $bytes . ' B';
        }

        $units = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $u = -1;
        do {
            $bytes /= $thresh;
            ++$u;
        } while (abs($bytes) >= $thresh && $u < count($units) - 1);

        return round($bytes, 1) . ' ' . $units[$u];
    }
}
