<?php

namespace Engine\FileManager\Service\File;

use Engine\FileManager\Entity\File;
use Engine\FileManager\Setting\Contract\Config;
use Engine\FileManager\Value\Path;

/**
 * Class RemoveTempFile
 * @package Engine\FileManager\Service\File
 */
class RemoveTempFile
{
    /**
     * @var Path
     */
    private $temporaryPath;

    /**
     * RemoveFile constructor.
     *
     */
    public function __construct(Config $config)
    {
        $this->temporaryPath = $config->getTemporaryPath();
    }

    public function execute(File $file)
    {
        if ($this->isTempFile($file)) {
            unlink($file->getPath());
        }
    }

    /**
     * @param File $file
     *
     * @return bool
     */
    private function isTempFile(File $file): bool
    {
        return $file->getPath()->within($this->temporaryPath) && file_exists($file->getPath());
    }
}
