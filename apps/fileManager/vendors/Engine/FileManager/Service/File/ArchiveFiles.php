<?php

namespace Engine\FileManager\Service\File;

use DomainException;
use Engine\FileManager\Entity\Directory;
use Engine\FileManager\Entity\File;
use Engine\FileManager\Value\Path;
use Engine\FileManager\Value\Size;
use ZipArchive;

/**
 * Class ArchiveFiles
 * @package Engine\FileManager\Service\File
 */
class ArchiveFiles
{
    /**
     * @param Path $archivePath
     * @param array $files
     *
     * @return File
     */
    public function execute(Path $archivePath, array $files)
    {
        if (empty($files)) {
            throw new DomainException('An attempt to create an empty archive');
        }

        $archive = new ZipArchive();
        $archive->open($archivePath, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        $this->populateArchive($archive, $files);
        $archive->close();

        return (new File())
            ->setPath($archivePath)
            ->setSize(new Size(filesize($archivePath)));
    }

    /**
     * @param ZipArchive $archive
     * @param File[] $files
     * @param Path|null $relativePath
     */
    private function populateArchive(ZipArchive $archive, array $files, Path $relativePath = null)
    {
        foreach ($files as $file) {
            $fileName = $file->getPath()->getFileName();
            $path = null === $relativePath ? new Path($fileName) : $relativePath->join($fileName);

            if ($file instanceof Directory) {
                if ($file->hasChildren()) {
                    $this->populateArchive($archive, $file->getChildren(), $path);
                } else {
                    $archive->addEmptyDir($path);
                }
                continue;
            }

            $archive->addFile($file->getPath(), $path);
        }
    }
}
