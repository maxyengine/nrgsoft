<?php

namespace Engine\FileManager\Service\File;

use DateTime;
use Engine\FileManager\Entity\Hyperlink;
use Engine\FileManager\Persistence\Contract\FileRepository;
use Engine\FileManager\Value\FileId;
use Engine\FileManager\Value\FileName;
use Engine\FileManager\Value\Permissions;
use Engine\FileManager\Value\Size;
use Engine\SDK\HTTP\Value\Url;

/**
 * Class CreateHyperlink
 * @package Engine\FileManager\Service\File
 */
class CreateHyperlink
{
    /**
     * @var FileRepository
     */
    private $repository;

    /**
     * CreateHyperlink constructor.
     *
     * @param FileRepository $repository
     */
    public function __construct(FileRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param FileName $name
     * @param FileId $parentId
     *
     * @return Hyperlink
     */
    public function execute(FileName $name, FileId $parentId, Url $url): Hyperlink
    {
        $parent = $this->repository->readFile($parentId);
        $name = $this->createFileName($name, $url);

        $file = (new Hyperlink())
            ->setId($parentId->join($name))
            ->setPath($parent->getPath()->join($name))
            ->setSize(new Size(mb_strlen((string)$url)))
            ->setType('hyperlink')
            ->setUrl($url)
            ->setPermissions(new Permissions(0755))
            ->setLastModified(new DateTime());

        $this->repository->saveFiles($file);

        return $file;
    }

    private function createFileName(FileName $name, Url $url)
    {
        return new FileName($name . '.' . ($url->getScheme() ?? 'http'));
    }
}
