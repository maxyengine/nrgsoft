<?php

namespace Engine\FileManager\Service\File;

use Engine\FileManager\Entity\Directory;
use Engine\FileManager\Persistence\Contract\FileRepository;
use Engine\FileManager\Setting\Contract\Config;
use Engine\FileManager\Value\FileId;
use Engine\FileManager\Value\Path;

/**
 * Class CreateDownloadFile
 * @package Engine\FileManager\Service\File
 */
class CreateDownloadFile
{
    /**
     * @var FileRepository
     */
    private $repository;

    /**
     * @var ArchiveFiles
     */
    private $archiveFiles;

    /**
     * @var Path
     */
    private $temporaryPath;

    /**
     * RemoveFile constructor.
     *
     */
    public function __construct(FileRepository $repository, ArchiveFiles $archiveFiles, Config $config)
    {
        $this->repository = $repository;
        $this->archiveFiles = $archiveFiles;
        $this->temporaryPath = $config->getTemporaryPath();
    }

    public function execute(FileId...$ids)
    {
        if (count($ids) === 1) {
            $file = $this->repository->readFile($ids[0], true);
            if ($file instanceof Directory) {
                $archivePath = $this->temporaryPath->join($file->getPath()->getFileName()->getBaseName() . '.zip');
                $file = $this->archiveFiles->execute($archivePath, (array)$file->getChildren());
            }
        } else {
            $files = [];
            foreach ($ids as $id) {
                $files[] = $this->repository->readFile($id, true);
            }
            $archivePath = $this->temporaryPath->join(date('Y-m-d H_i_s') . '.zip');
            return $this->archiveFiles->execute($archivePath, $files);
        }

        return $file;
    }
}
