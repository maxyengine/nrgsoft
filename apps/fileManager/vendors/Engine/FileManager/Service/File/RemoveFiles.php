<?php

namespace Engine\FileManager\Service\File;

use Engine\FileManager\Entity\FileCollection;
use Engine\FileManager\Persistence\Contract\FileRepository;
use Engine\FileManager\Value\FileId;

/**
 * Class RemoveFiles
 * @package Engine\FileManager\Service\File
 */
class RemoveFiles
{
    /**
     * @var FileRepository
     */
    private $repository;

    /**
     * RemoveFile constructor.
     *
     * @param FileRepository $repository
     */
    public function __construct(FileRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param FileId[] ...$ids
     *
     * @return FileCollection
     */
    public function execute(FileId...$ids): FileCollection
    {
        $files = new FileCollection();

        foreach ($ids as $id) {
            $files->addFile($this->repository->readFile($id));
        }

        $this->repository->removeFiles(...$files);

        return $files;
    }
}
