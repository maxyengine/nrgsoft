<?php

namespace Engine\FileManager\Service\File;

use Engine\FileManager\Entity\Directory;
use Engine\FileManager\Persistence\Contract\FileRepository;
use Engine\FileManager\Value\FileId;
use Engine\FileManager\Entity\File;

/**
 * Class ReadFile
 * @package Engine\FileManager\Service\File
 */
class ReadFile
{
    /**
     * @var FileRepository
     */
    private $repository;

    /**
     * ReadFile constructor.
     *
     * @param FileRepository $repository
     */
    public function __construct(FileRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param FileId $id
     * @param bool $recursively
     *
     * @return Directory|File
     */
    public function execute(FileId $id, bool $recursively = null): File
    {
        return $this->repository->readFile($id, $recursively);
    }
}
