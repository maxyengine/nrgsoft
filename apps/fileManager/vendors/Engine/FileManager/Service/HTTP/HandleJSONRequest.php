<?php

namespace Engine\FileManager\Service\HTTP;

use Engine\SDK\HTTP\Value\Request;
use Engine\SDK\HTTP\Value\Response;

class HandleJSONRequest
{
    /**
     * @param Request $request
     * @param Response $response
     */
    public function execute(Request $request, Response $response)
    {
        $bodyParams = json_decode($request->getBody(), true) ?? [];
        $request->setBodyParams($bodyParams + $request->getBodyParams());
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');
    }
}
