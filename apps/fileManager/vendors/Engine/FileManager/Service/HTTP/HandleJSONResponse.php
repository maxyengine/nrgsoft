<?php

namespace Engine\FileManager\Service\HTTP;

use Engine\FileManager\View\Factory;
use Engine\SDK\HTTP\Value\Response;

class HandleJSONResponse
{
    /**
     * @var Factory
     */
    private $factory;

    /**
     * MapJSONResponse constructor.
     *
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param Response $response
     */
    public function execute(Response $response)
    {
        if ($response->containsInHeader('Content-Type', 'application/json')) {
            $response->setBody(json_encode($this->factory->createView($response->getBody()),
                JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
        }
    }
}
