<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Entity\File;
use Engine\FileManager\Value\FileId;
use Engine\FileManager\Service\File\ReadFile;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

/**
 * Class Open
 * @package Engine\FileManager\Action
 */
class Open implements Observer
{
    use ObserverStub;

    /**
     * @var ReadFile
     */
    private $readFile;

    /**
     * @var File|null
     */
    private $file;

    /**
     * Read constructor.
     *
     * @param ReadFile $readFile
     */
    public function __construct(ReadFile $readFile)
    {
        $this->readFile = $readFile;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $id = new FileId($event->getRequest()->getQueryParam('id'));

        $this->file = $this->readFile->execute($id);

        $event->getResponse()->setHeader('Content-Type', $this->file->getType());
    }

    public function onComplete()
    {
        if (ob_get_level()) {
            ob_end_clean();
        }

        readfile($this->file->getPath());
    }
}
