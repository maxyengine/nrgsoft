<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Service\File\MoveFiles;
use Engine\FileManager\Value\FileId;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

/**
 * Class Trash
 * @package Engine\FileManager\Action
 */
class Trash implements Observer
{
    use ObserverStub;

    /**
     * @var MoveFiles
     */
    private $moveFiles;

    /**
     * Trash constructor.
     *
     * @param MoveFiles $trashFiles
     */
    public function __construct(MoveFiles $trashFiles)
    {
        $this->moveFiles = $trashFiles;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $parentId = new FileId('/$trash');
        $ids = $event->getRequest()->getBodyParam('id');

        if (!is_array($ids)) {
            $ids = [$ids];
        }

        foreach ($ids as &$id) {
            $id = new FileId($id);
        }

        $files = $this->moveFiles->execute($parentId, ...$ids);

        $event->getResponse()->setBody($files);
    }
}
