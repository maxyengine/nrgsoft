<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Persistence\Exception\NotFoundException;
use Engine\FileManager\Service\File\ReadFile;
use Engine\FileManager\Value\FileId;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\HTTP\Value\Status;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

/**
 * Class Read
 * @package Engine\FileManager\Action
 */
class Read implements Observer
{
    use ObserverStub;

    /**
     * @var ReadFile
     */
    private $readFile;

    /**
     * Read constructor.
     *
     * @param ReadFile $readFile
     */
    public function __construct(ReadFile $readFile)
    {
        $this->readFile = $readFile;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $id = new FileId($event->getRequest()->getBodyParam('id'));

        try {
            $file = $this->readFile->execute($id, false);
            $event->getResponse()->setBody($file);
        } catch (NotFoundException $exception) {
            $event->getResponse()->setStatus(new Status(404, $exception->getMessage()));
        }
    }
}
