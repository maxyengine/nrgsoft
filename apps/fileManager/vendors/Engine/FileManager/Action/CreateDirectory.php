<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Service\File\CreateDirectory as Service;
use Engine\FileManager\Value\FileId;
use Engine\FileManager\Value\FileName;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

/**
 * Class CreateDirectory
 * @package Engine\FileManager\Action
 */
class CreateDirectory implements Observer
{
    use ObserverStub;

    /**
     * @var Service
     */
    private $createDirectory;

    /**
     * CreateDirectory constructor.
     *
     * @param Service $createDirectory
     */
    public function __construct(Service $createDirectory)
    {
        $this->createDirectory = $createDirectory;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $parentId = new FileId($event->getRequest()->getBodyParam('parentId'));
        $name = new FileName($event->getRequest()->getBodyParam('name'), true);

        $file = $this->createDirectory->execute($name, $parentId);

        $event->getResponse()->setBody($file);
    }
}
