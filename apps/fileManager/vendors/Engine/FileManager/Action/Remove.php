<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Service\File\RemoveFiles;
use Engine\FileManager\Value\FileId;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

/**
 * Class Remove
 * @package Engine\FileManager\Action
 */
class Remove implements Observer
{
    use ObserverStub;

    /**
     * @var RemoveFiles
     */
    private $removeFiles;

    /**
     * Remove constructor.
     *
     * @param RemoveFiles $removeFiles
     */
    public function __construct(RemoveFiles $removeFiles)
    {
        $this->removeFiles = $removeFiles;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $ids = $event->getRequest()->getBodyParam('id');

        if (!is_array($ids)) {
            $ids = [$ids];
        }

        foreach ($ids as &$id) {
            $id = new FileId($id);
        }

        $files = $this->removeFiles->execute(...$ids);

        $event->getResponse()->setBody($files);
    }
}
