<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Service\File\SetFilePermissions;
use Engine\FileManager\Value\FileId;
use Engine\FileManager\Value\Permissions;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

/**
 * Class SetPermissions
 * @package Engine\FileManager\Action
 */
class SetPermissions implements Observer
{
    use ObserverStub;

    /**
     * @var SetFilePermissions
     */
    private $setFilePermissions;

    /**
     * SetPermissions constructor.
     *
     * @param SetFilePermissions $setFilePermissions
     */
    public function __construct(SetFilePermissions $setFilePermissions)
    {
        $this->setFilePermissions = $setFilePermissions;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $id = new FileId($event->getRequest()->getBodyParam('id'));
        $permissions = Permissions::createOfOctalString($event->getRequest()->getBodyParam('permissions'));

        $file = $this->setFilePermissions->execute($id, $permissions);

        $event->getResponse()->setBody($file);
    }
}
