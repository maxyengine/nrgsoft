<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Value\FileId;
use Engine\FileManager\Service\File\UploadFile;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

/**
 * Class Upload
 * @package Engine\FileManager\Action
 */
class Upload implements Observer
{
    use ObserverStub;

    /**
     * @var UploadFile
     */
    private $uploadFile;

    /**
     * Upload constructor.
     *
     * @param UploadFile $uploadFile
     */
    public function __construct(UploadFile $uploadFile)
    {
        $this->uploadFile = $uploadFile;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $uploadedFile = $event->getRequest()->getUploadedFiles()['file'];
        $parentId = new FileId($event->getRequest()->getBodyParam('parentId'));

        $file = $this->uploadFile->execute($uploadedFile, $parentId);

        $event->getResponse()->setBody($file);
    }
}
