<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Service\File\RenameFile;
use Engine\FileManager\Value\FileId;
use Engine\FileManager\Value\FileName;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

class Rename implements Observer
{
    use ObserverStub;

    /**
     * @var RenameFile
     */
    private $renameFile;

    /**
     * Read constructor.
     *
     * @param RenameFile $renameFile
     */
    public function __construct(RenameFile $renameFile)
    {
        $this->renameFile = $renameFile;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $id = new FileId($event->getRequest()->getBodyParam('id'));
        $name = new FileName($event->getRequest()->getBodyParam('name'));

        $file = $this->renameFile->execute($id, $name);

        $event->getResponse()->setBody($file);
    }
}
