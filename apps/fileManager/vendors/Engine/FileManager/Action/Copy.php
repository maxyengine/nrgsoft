<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Service\File\CopyFiles;
use Engine\FileManager\Value\FileId;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

/**
 * Class Copy
 * @package Engine\FileManager\Action
 */
class Copy implements Observer
{
    use ObserverStub;

    /**
     * @var CopyFiles
     */
    private $copyFiles;

    /**
     * Copy constructor.
     *
     * @param CopyFiles $copyFiles
     */
    public function __construct(CopyFiles $copyFiles)
    {
        $this->copyFiles = $copyFiles;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $parentId = new FileId($event->getRequest()->getBodyParam('parentId'));
        $ids = $event->getRequest()->getBodyParam('id');

        if (!is_array($ids)) {
            $ids = [$ids];
        }

        foreach ($ids as &$id) {
            $id = new FileId($id);
        }

        $files = $this->copyFiles->execute($parentId, ...$ids);

        $event->getResponse()->setBody($files);
    }
}
