<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Service\File\SearchFiles;
use Engine\FileManager\Value\FileId;
use Engine\FileManager\Value\Pattern;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

/**
 * Class Search
 * @package Engine\FileManager\Action
 */
class Search implements Observer
{
    use ObserverStub;

    /**
     * @var SearchFiles
     */
    private $searchFiles;

    /**
     * Search constructor.
     *
     * @param SearchFiles $searchFiles
     */
    public function __construct(SearchFiles $searchFiles)
    {
        $this->searchFiles = $searchFiles;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $id = new FileId($event->getRequest()->getBodyParam('id'));
        $pattern = new Pattern($event->getRequest()->getBodyParam('pattern'));

        $file = $this->searchFiles->execute($id, $pattern);

        $event->getResponse()->setBody($file);
    }
}
