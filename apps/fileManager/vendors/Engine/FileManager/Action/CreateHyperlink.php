<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Service\File\CreateHyperlink as Service;
use Engine\FileManager\Value\FileId;
use Engine\FileManager\Value\FileName;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\HTTP\Value\Url;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

/**
 * Class CreateHyperlink
 * @package Engine\FileManager\Action
 */
class CreateHyperlink implements Observer
{
    use ObserverStub;

    /**
     * @var Service
     */
    private $createHyperlink;

    /**
     * CreateHyperlink constructor.
     *
     * @param Service $createHyperlink
     */
    public function __construct(Service $createHyperlink)
    {
        $this->createHyperlink = $createHyperlink;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $parentId = new FileId($event->getRequest()->getBodyParam('parentId'));
        $name = new FileName($event->getRequest()->getBodyParam('name'));
        $url = new Url($event->getRequest()->getBodyParam('url'));

        $file = $this->createHyperlink->execute($name, $parentId, $url);

        $event->getResponse()->setBody($file);
    }
}
