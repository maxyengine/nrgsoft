<?php

namespace Engine\FileManager\Action;

use Engine\FileManager\Service\File\MoveFiles;
use Engine\FileManager\Value\FileId;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

/**
 * Class Move
 * @package Engine\FileManager\Action
 */
class Move implements Observer
{
    use ObserverStub;

    /**
     * @var MoveFiles
     */
    private $moveFiles;

    /**
     * Move constructor.
     *
     * @param MoveFiles $moveFiles
     */
    public function __construct(MoveFiles $moveFiles)
    {
        $this->moveFiles = $moveFiles;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $parentId = new FileId($event->getRequest()->getBodyParam('parentId'));
        $ids = $event->getRequest()->getBodyParam('id');

        if (!is_array($ids)) {
            $ids = [$ids];
        }

        foreach ($ids as &$id) {
            $id = new FileId($id);
        }

        $files = $this->moveFiles->execute($parentId, ...$ids);

        $event->getResponse()->setBody($files);
    }
}
