<?php

namespace Engine\FileManager\View;

use Traversable;

class Factory
{
    public function __construct(array $views)
    {
        $this->views = $views;
    }

    public function createView($raw)
    {
        if (null === $raw || is_array($raw)) {
            return $raw;
        }

        if ($raw instanceof Traversable) {
            $result = [];
            foreach ($raw as $entity) {
                $result[] = new $this->views[get_class($entity)]($entity, $this);
            }

            return $result;
        }

        return new $this->views[get_class($raw)]($raw, $this);
    }
}
