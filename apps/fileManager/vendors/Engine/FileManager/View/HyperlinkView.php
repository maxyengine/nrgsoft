<?php

namespace Engine\FileManager\View;

/**
 * Class HyperlinkView
 * @package Engine\FileManager\View
 */
class HyperlinkView extends FileView
{
    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
                'url' => (string)$this->getUrl(),
            ] + parent::jsonSerialize();
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return 'hyperlink';
    }

    public function getUrl()
    {
        return $this->file->getUrl();
    }
}
