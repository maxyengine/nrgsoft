<?php

namespace Engine\FileManager\Persistence\Contract;

use stdClass;

/**
 * Interface FileAdapter
 * @package Engine\FileManager\Persistence\Contract
 */
interface FileAdapter
{
    public function readFile(string $id, bool $recursively = null): stdClass;

    public function searchFiles(string $id, string $pattern): array;

    public function saveFile(stdClass $raw);

    public function copyFile(stdClass $raw);

    public function moveFile(stdClass $raw);

    public function removeFile(string $id);

    public function exists(string $id): bool;

    public function getFileSize(string $id): int;
}
