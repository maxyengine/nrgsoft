<?php

namespace Engine\FileManager\Persistence\Contract;

use Engine\FileManager\Entity\File;
use stdClass;

/**
 * Interface FileFactory
 * @package Engine\FileManager\Persistence\Contract
 */
interface FileFactory
{
    /**
     * @param stdClass $raw
     *
     * @return File
     */
    public function createFile(stdClass $raw): File;

    /**
     * @param File $file
     *
     * @return stdClass
     */
    public function recycleFile(File $file): stdClass;
}
