<?php

namespace Engine\FileManager\Persistence\Contract;

use Engine\FileManager\Entity\Directory;
use Engine\FileManager\Entity\File;
use Engine\FileManager\Entity\FileCollection;
use Engine\FileManager\Value\FileId;
use Engine\FileManager\Value\Pattern;
use Engine\FileManager\Value\Size;

interface FileRepository
{
    /**
     * @param FileId $id
     * @param bool|null $recursively
     *
     * @return File|Directory
     */
    public function readFile(FileId $id, bool $recursively = null): File;

    public function searchFiles(FileId $id, Pattern $pattern): FileCollection;

    public function saveFiles(File ...$files);

    public function copyFiles(File ...$files);

    public function moveFiles(File ...$files);

    public function removeFiles(File ...$files);

    public function exists(FileId $id): bool;

    public function getFileSize(FileId $id): Size;
}
