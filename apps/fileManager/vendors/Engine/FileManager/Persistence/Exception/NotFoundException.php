<?php

namespace Engine\FileManager\Persistence\Exception;

use DomainException;

class NotFoundException extends DomainException
{

}