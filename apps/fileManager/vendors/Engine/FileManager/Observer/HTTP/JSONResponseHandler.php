<?php

namespace Engine\FileManager\Observer\HTTP;

use Engine\FileManager\Service\HTTP\HandleJSONResponse;
use Engine\FileManager\View\Factory;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

class JSONResponseHandler implements Observer
{
    use ObserverStub;

    /**
     * @var HandleJSONResponse
     */
    private $handleJSONResponse;

    public function __construct(array $views)
    {
        $this->handleJSONResponse = new HandleJSONResponse(new Factory($views));
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->handleJSONResponse->execute($event->getResponse());
    }
}
