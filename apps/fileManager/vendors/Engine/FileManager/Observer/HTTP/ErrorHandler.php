<?php

namespace Engine\FileManager\Observer\HTTP;

use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\HTTP\Value\Status;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;
use Throwable;

class ErrorHandler implements Observer
{
    use ObserverStub;

    /**
     * @param Throwable $throwable
     * @param ExchangeEvent $event
     */
    public function onError(Throwable $throwable, $event)
    {
        $event->getResponse()->setStatus(new Status(500, $throwable->getMessage()));
    }
}
