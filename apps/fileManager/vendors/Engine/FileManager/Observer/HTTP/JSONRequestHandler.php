<?php

namespace Engine\FileManager\Observer\HTTP;

use Engine\FileManager\Service\HTTP\HandleJSONRequest;
use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;

class JSONRequestHandler implements Observer
{
    use ObserverStub;

    /**
     * @var HandleJSONRequest
     */
    private $handleJSONRequest;

    public function __construct()
    {
        $this->handleJSONRequest = new HandleJSONRequest();
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->handleJSONRequest->execute($event->getRequest(), $event->getResponse());
    }
}
