<?php

namespace Engine\FileManager;

use Engine\FileManager\Observer\HTTP\ErrorHandler;
use Engine\FileManager\Observer\HTTP\JSONRequestHandler;
use Engine\FileManager\Observer\HTTP\JSONResponseHandler;
use Engine\FileManager\Setting\ArrayConfig;
use Engine\FileManager\Setting\Contract\Config;
use Engine\SDK\DI\Injector;
use Engine\SDK\HTTP\Value\Request;
use Engine\SDK\HTTP\Value\Response;
use Engine\SDK\HTTP\Observer\CGIRequestMaker;
use Engine\SDK\HTTP\Observer\ResponseEmitter;
use Engine\SDK\HTTP\Observer\RequestedActionRunner;
use Engine\SDK\HTTP\Server;
use Engine\SDK\RX\Observable;
use Engine\SDK\Utility\ErrorToExceptionTranslator;

/**
 * Class Application
 * @package Engine\FileManager
 */
class Application
{
    /**
     * @var Server
     */
    private $server;

    /**
     * @var Observable
     */
    private $eventResource;

    /**
     * Application constructor.
     *
     * @param array $config
     * @param array $routes
     * @param array $services
     * @param array $views
     */
    public function __construct(array $config, array $routes, array $services, array $views)
    {
        $errorTranslator = new ErrorToExceptionTranslator();
        $errorTranslator->enable();

        $injector = new Injector();
        $injector
            ->setService(Config::class, new ArrayConfig($config))
            ->loadServices($services);

        $this->eventResource = new Observable();
        $this->eventResource
            ->addObserver(new ErrorHandler())
            ->addObserver(new CGIRequestMaker())
            ->addObserver(new JSONRequestHandler())
            ->addObserver(new RequestedActionRunner($injector, $routes))
            ->addObserver(new JSONResponseHandler($views))
            ->addObserver(new ResponseEmitter());

        $this->server = new Server();
    }

    /**
     * Runs application.
     */
    public function run()
    {
        $this->server->exchange(new Request(), new Response(), $this->eventResource);
    }
}
