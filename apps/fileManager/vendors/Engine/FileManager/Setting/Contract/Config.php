<?php

namespace Engine\FileManager\Setting\Contract;

use Engine\FileManager\Value\Path;
use Engine\FileManager\Value\Size;
use Engine\SDK\Utility\Contract\BaseConfig;

interface Config extends BaseConfig
{
    public function readPublicConfig(): BaseConfig;

    public function writePublicConfig(BaseConfig $config);

    public function getStoragePath(): Path;

    public function getTemporaryPath(): Path;

    public function getStorageMaxSize(): Size;

    public function getUploadMaxFileSize(): Size;

    public function isFileSizeAllowed(int $size): bool;

    public function isMimeTypeAllowed(string $type): bool;
}