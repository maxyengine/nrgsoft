<?php

namespace Engine\FileManager\Entity;

use ArrayIterator;
use IteratorAggregate;

/**
 * Class FileCollection
 * @package Engine\FileManager\Entity
 */
class FileCollection implements IteratorAggregate
{
    /**
     * @var File[]
     */
    private $files = [];

    /**
     * @param File $file
     */
    public function addFile(File $file)
    {
        $this->files[] = $file;
    }

    public function getIterator()
    {
        return new ArrayIterator($this->files);
    }
}
