<?php

namespace Engine\FileManager\Entity;

use Engine\SDK\HTTP\Value\Url;

/**
 * Class Hyperlink
 * @package Engine\FileManager\Entity
 */
class Hyperlink extends File
{
    /**
     * @var Url|null
     */
    private $url;

    /**
     * @return Url|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param Url|null $url
     *
     * @return Hyperlink
     */
    public function setUrl(Url $url)
    {
        $this->url = $url;

        return $this;
    }
}
