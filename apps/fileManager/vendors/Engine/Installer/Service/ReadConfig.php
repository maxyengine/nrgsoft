<?php

namespace Engine\Installer\Service;

use Engine\FileManager\Setting\Contract\Config;
use Engine\SDK\Utility\Contract\BaseConfig;

/**
 * Class ReadConfig
 * @package Engine\Installer\Service
 */
class ReadConfig
{
    /**
     * @var Config
     */
    private $config;

    /**
     * ReadConfig constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return BaseConfig
     */
    public function execute(): BaseConfig
    {
        return $this->config->readPublicConfig();
    }
}
