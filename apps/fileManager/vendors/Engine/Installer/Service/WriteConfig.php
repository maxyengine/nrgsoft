<?php

namespace Engine\Installer\Service;

use Engine\FileManager\Setting\Contract\Config;
use Engine\SDK\Utility\Contract\BaseConfig;

/**
 * Class WriteConfig
 * @package Engine\Installer\Service
 */
class WriteConfig
{
    /**
     * @var Config
     */
    private $config;

    /**
     * WriteConfig constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function execute(BaseConfig $config)
    {
        $this->config->writePublicConfig($config);
    }
}
