<?php

namespace Engine\Installer\Action;

use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;
use Engine\Installer\Service\WriteConfig as Service;
use Engine\SDK\Utility\ArrayBaseConfig;

/**
 * Class WriteConfig
 * @package Engine\Installer\Action
 */
class WriteConfig implements Observer
{
    use ObserverStub;

    /**
     * @var Service
     */
    private $writeConfig;

    /**
     * WriteConfig constructor.
     *
     * @param Service $writeConfig
     */
    public function __construct(Service $writeConfig)
    {
        $this->writeConfig = $writeConfig;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $config = new ArrayBaseConfig((array)$event->getRequest()->getBodyParams());

        $this->writeConfig->execute($config);

        $event->getResponse()->setBody($config->asArray());
    }
}
