<?php

namespace Engine\Installer\Action;

use Engine\SDK\HTTP\Event\ExchangeEvent;
use Engine\SDK\RX\Contract\Observer;
use Engine\SDK\RX\ObserverStub;
use Engine\Installer\Service\ReadConfig as Service;

/**
 * Class ReadConfig
 * @package Engine\Installer\Action
 */
class ReadConfig implements Observer
{
    use ObserverStub;

    /**
     * @var Service
     */
    private $readConfig;

    /**
     * ReadConfig constructor.
     *
     * @param Service $readConfig
     */
    public function __construct(Service $readConfig)
    {
        $this->readConfig = $readConfig;
    }

    /**
     * @param ExchangeEvent $event
     */
    public function onNext($event)
    {
        $config = $this->readConfig->execute();

        $event->getResponse()->setBody($config->asArray());
    }
}
