<?php

use Engine\FileManager\Application;

require __DIR__ . '/vendors/classloader.php';

(new Application(
    require __DIR__ . '/settings/config.php',
    require __DIR__ . '/settings/routes.php',
    require __DIR__ . '/settings/services.php',
    require __DIR__ . '/settings/views.php'
))->run();
