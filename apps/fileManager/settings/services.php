<?php

use Engine\FileManager\Persistence\Contract\FileAdapter;
use Engine\FileManager\Persistence\Contract\FileFactory;
use Engine\FileManager\Persistence\Contract\FileRepository;
use Engine\FileManager\Persistence\IO\IOFileAdapter;
use Engine\FileManager\Persistence\IO\IOFileFactory;
use Engine\FileManager\Persistence\IO\IOFileRepository;

return [
    FileAdapter::class => IOFileAdapter::class,
    FileFactory::class => IOFileFactory::class,
    FileRepository::class => IOFileRepository::class,
];
