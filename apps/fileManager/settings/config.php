<?php

session_start();

if (!file_exists(__DIR__ . '/public.php')) {
    copy(__DIR__ . '/public-default.php', __DIR__ . '/public.php');
}

$config = array_merge(
    require __DIR__ . '/public.php',
    require __DIR__ . '/private.php'
);

if (!isset($_SESSION['engine-fm'])) {
    $dailyDirectory = __DIR__ . '/../demo-roots/' . date('Y-m-d');
    if (!is_dir($dailyDirectory)) {
        mkdir($dailyDirectory);
    }

    $milliseconds = round(microtime(true) * 1000);
    $root = $dailyDirectory . DIRECTORY_SEPARATOR . $milliseconds;

    mkdir($root);

    $_SESSION['engine-fm'] = [
        'root' => realpath($root),
    ];
}

$config['storagePath'] = $_SESSION['engine-fm']['root'];

return $config;

?>
